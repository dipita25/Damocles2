package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import cm.darttc.damocles.model.Matiere;

/**
 * 
 */
@Stateless
@Path("/matieres")
public class MatiereEndpoint {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;

	@POST
	@Path("/save")
	@Consumes("application/json")
	public Response create(Matiere entity) {
		em.persist(entity);
		return Response.created(
				UriBuilder.fromResource(MatiereEndpoint.class)
						.path(String.valueOf(entity.getId())).build()).build();
	}

	@DELETE
	@Path("/delete")
	public Response deleteById(Long id) {
		Matiere entity = em.find(Matiere.class, id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		em.remove(entity);
		return Response.noContent().build();
	}

	@POST
	@Path("/get")
	@Produces("application/json")
	public Response findById(Long id) {
		TypedQuery<Matiere> findByIdQuery = em
				.createQuery(
						"SELECT DISTINCT m FROM Matiere m WHERE m.id = :entityId ORDER BY m.id",
						Matiere.class);
		findByIdQuery.setParameter("entityId", id);
		Matiere entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			entity = null;
		}
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Path("/")
	@Produces("application/json")
	public List<Matiere> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		TypedQuery<Matiere> findAllQuery = em
				.createQuery("SELECT DISTINCT m FROM Matiere m ORDER BY m.id",
						Matiere.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<Matiere> results = findAllQuery.getResultList();
		return results;
	}

	@PUT
	@Path("/update")
	@Consumes("application/json")
	public Response update(Matiere entity) {
		
		Long id = entity.getId();
		
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (em.find(Matiere.class, id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = em.merge(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
}
