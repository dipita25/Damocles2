package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.services.Nbre_seanceService;

@Path("/nbre_seance")
public class Nbre_seanceEndpoint {
	
	@EJB
	private Nbre_seanceService nbre_seanceService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(ApprenantDTO apprenantDTO) {
		
		try{
			int[] seances = nbre_seanceService.recherche(apprenantDTO);
			return Response.ok(seances).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}

}
