package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.UtilisateurDTO;
import cm.darttc.damocles.model.Apprenant;
import cm.darttc.damocles.services.ApprenantsParentService;

@Path("/apprenantsParent")
public class ApprenantsParentEndpoint {

	
	@EJB
	private ApprenantsParentService apprenantsParentService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(UtilisateurDTO utilisateurDTO) {
		
		try{
			List<Apprenant> apprenants = apprenantsParentService.recherche(utilisateurDTO);
			return Response.ok(apprenants).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}
}
