package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.services.MatiereRepetitionService;

@Path("/mesMatieres")
public class MatiereRepetitionEndpoint {


	@EJB
	private MatiereRepetitionService matiereRepetitionService;
	
	@POST
	@Consumes("application/json")
	public Response mesMatieres(ApprenantDTO apprenantDTO) {
		try{
			
			List<Matiere> matieres = matiereRepetitionService.mesMatieres(apprenantDTO);
			
			return Response.ok(matieres).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
	}
}
