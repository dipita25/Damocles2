package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.UtilisateurDTO;
import cm.darttc.damocles.model.Apprenant;
import cm.darttc.damocles.services.NouveauPlanningService;

@Path("/mesApprenants")
public class NouveauPlanningEndpoint {
	
	@EJB
	private NouveauPlanningService nouveauPlanningService;
	
	@POST
	@Consumes("application/json")
	public Response mesApprenants(UtilisateurDTO utilisateurDTO) {
		try{
			
			List<Apprenant> apprenants = nouveauPlanningService.mesApprenants(utilisateurDTO);
			
			return Response.ok(apprenants).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
	}

}
