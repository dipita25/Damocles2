package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.SeanceMobileDTO;
import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.services.NouvelleSeanceMobileService;

@Path("/newSeanceMobile")
public class NouvelleSeanceMobileEndpoint {
	
	@EJB
	private NouvelleSeanceMobileService nouvelleSeanceMobileService;
	
	@POST
	@Produces("application/json")
	public Response enregistrer(SeanceMobileDTO seanceMobileDTO) {
		
		try{
			Seance seance = new Seance();
			seance = nouvelleSeanceMobileService.enregistrer(seanceMobileDTO);
			return Response.ok(seance).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}

}
