package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import cm.darttc.damocles.model.Repetition_Apprenant;
import cm.darttc.damocles.services.RepetitionApprenantService;

/**
 * 
 */
@Path("/repetition_apprenants")
public class Repetition_ApprenantEndpoint {
	
	@EJB
	private RepetitionApprenantService repetitionApprenantService;

	@POST
	@Consumes("application/json")
	public Response create(Repetition_Apprenant entity) {
		repetitionApprenantService.create(entity);
		return Response.created(
				UriBuilder.fromResource(Repetition_ApprenantEndpoint.class)
						.path(String.valueOf(entity.getId())).build()).build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		Repetition_Apprenant entity = repetitionApprenantService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		repetitionApprenantService.deleteById(id);
		return Response.noContent().build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		Repetition_Apprenant entity = repetitionApprenantService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<Repetition_Apprenant> listAll(
			@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		
		final List<Repetition_Apprenant> results = repetitionApprenantService.findRepetition_Apprenants();
		return results;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public Response update(@PathParam("id") Long id, Repetition_Apprenant entity) {
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (repetitionApprenantService.findById(id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = repetitionApprenantService.update(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
}
