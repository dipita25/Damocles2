package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.MatiereApprenantDTO;
import cm.darttc.damocles.services.AjoutMatiereApprenantService;

@Path("/ajoutMatiereApprenant")
public class AjoutMatiereApprenantEndpoint {

	@EJB
	private AjoutMatiereApprenantService ajoutMatiereApprenantService;
	
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	@POST
	@Produces("application/json")
	public Response ajoutMatiere(MatiereApprenantDTO matiereApprenantDTO){
		
		try{
			ajoutMatiereApprenantService.ajoutMatiere(matiereApprenantDTO);
			return Response.ok().build();
			
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}
	
}
