package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;
import cm.darttc.damocles.model.Seance;

/**
 * 
 */
@Stateless
@Path("/seances")
public class SeanceEndpoint {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;

	@POST
	@Consumes("application/json")
	public Response create(Seance entity) {
		em.persist(entity);
		return Response.created(
				UriBuilder.fromResource(SeanceEndpoint.class)
						.path(String.valueOf(entity.getId())).build()).build();
	}

	@DELETE
	@Path("/delete")
	public Response deleteById(Long id) {
		Seance entity = em.find(Seance.class, id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		em.remove(entity);
		return Response.noContent().build();
	}

	@POST
	@Path("/get")
	@Produces("application/json")
	public Response findById(Long id) {
		TypedQuery<Seance> findByIdQuery = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s LEFT JOIN FETCH s.repetition WHERE s.id = :entityId ORDER BY s.id",
						Seance.class);
		findByIdQuery.setParameter("entityId", id);
		Seance entity;
		try {
			entity = findByIdQuery.getSingleResult();
		} catch (NoResultException nre) {
			entity = null;
		}
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<Seance> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		TypedQuery<Seance> findAllQuery = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s LEFT JOIN FETCH s.repetition ORDER BY s.id",
						Seance.class);
		if (startPosition != null) {
			findAllQuery.setFirstResult(startPosition);
		}
		if (maxResult != null) {
			findAllQuery.setMaxResults(maxResult);
		}
		final List<Seance> results = findAllQuery.getResultList();
		return results;
	}

	@PUT
	@Path("/update")
	@Consumes("application/json")
	public Response update(Seance entity) {
		
		Long id = entity.getId();
		
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (em.find(Seance.class, id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = em.merge(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
}
