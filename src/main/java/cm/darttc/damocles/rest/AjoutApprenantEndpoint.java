package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Repetition_Apprenant;
import cm.darttc.damocles.services.AjoutApprenantService;

@Path("/ajoutApprenant")
public class AjoutApprenantEndpoint {

	
	@EJB
	private AjoutApprenantService ajoutApprenantService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(ApprenantDTO apprenantDTO) {
		
		try{
			Repetition_Apprenant repetition_Apprenant = ajoutApprenantService.enregistrer(apprenantDTO);
			return Response.ok(repetition_Apprenant).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}
}
