package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.FeedbackMobileDTO;
import cm.darttc.damocles.model.Feedback;
import cm.darttc.damocles.services.EnregistrerFeedbackMobileService;


@Path("/enregistrerFeedbackMobile")
public class EnregistrerFeedbackMobileEndpoint {
	
	@EJB
	private EnregistrerFeedbackMobileService enregistrerFeedbackMobileService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(FeedbackMobileDTO feedbackMobileDTO) {
		
		try{
			Feedback feedback = enregistrerFeedbackMobileService.enregistrer(feedbackMobileDTO);
			return Response.ok(feedback).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}

}
