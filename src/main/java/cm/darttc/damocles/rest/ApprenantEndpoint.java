package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Apprenant;
import cm.darttc.damocles.model.Parent;
import cm.darttc.damocles.services.ApprenantService;
import cm.darttc.damocles.services.ParentService;

/**
 * 
 */
@Path("/apprenants")
public class ApprenantEndpoint {
	
	@EJB
	private ApprenantService apprenantService;
	
	@EJB
	private ParentService parentService;

	@POST
	@Consumes("application/json")
	public Response create(Apprenant entity) {
		apprenantService.create(entity);
		return Response.created(
				UriBuilder.fromResource(ApprenantEndpoint.class)
						.path(String.valueOf(entity.getId())).build()).build();
	}
	

	@DELETE
	//@Path("/{id:[0-9][0-9]*}")
	@Path("/delete")
	public Response deleteById(Long id) {
		Apprenant entity = apprenantService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		apprenantService.deleteById(id);
		return Response.noContent().build();
	}

	@POST
	@Path("/get")
	@Produces("application/json")
	public Response findById(Long id) {
		
		Apprenant entity = apprenantService.findById(id);
		
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<Apprenant> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		final List<Apprenant> results = apprenantService.findAdmins();
		return results;
	}

	@PUT
	//@Path("/{id:[0-9][0-9]*}")
	@Path("/update")
	@Consumes("application/json")
	public Response update(Apprenant element) {
		Apprenant entity = new Apprenant();
		Long id = element.getId();
		
		entity.setVersion(element.getVersion());
		entity.setAge(element.getAge());
		entity.setClasse(element.getClasse());
		entity.setId(element.getId());
		entity.setEtablissement(element.getEtablissement());
		entity.setNom(element.getNom());
		entity.setPrenom(element.getPrenom());
		
		Parent parent = parentService.findById(element.getParent().getId());
		entity.setParent(parent);
		
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (apprenantService.findById(id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = apprenantService.update(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
}
