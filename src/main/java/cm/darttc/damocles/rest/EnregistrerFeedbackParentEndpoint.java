package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.FeedbackDTO;
import cm.darttc.damocles.model.Feedback;
import cm.darttc.damocles.services.EnregistrerFeedbackParentService;

@Path("/enregistrerFeedbackParent")
public class EnregistrerFeedbackParentEndpoint {
	
	@EJB
	private EnregistrerFeedbackParentService enregistrerFeedbackParentService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(FeedbackDTO feedbackDTO) {
		
		try{
			Feedback feedback = enregistrerFeedbackParentService.enregistrer(feedbackDTO);
			return Response.ok(feedback).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}

}
