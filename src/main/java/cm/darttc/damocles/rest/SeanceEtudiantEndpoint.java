package cm.darttc.damocles.rest;


import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.services.SeanceEtudiantService;

@Path("/SeanceEtudiant")
public class SeanceEtudiantEndpoint {
	
	@EJB
	private SeanceEtudiantService seanceEtudiantService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(ApprenantDTO apprenantDTO) {
		
		try{
			List<Seance> seances = seanceEtudiantService.recherche(apprenantDTO);
			return Response.ok(seances).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}
}
