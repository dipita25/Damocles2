package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import cm.darttc.damocles.DTO.InscriptionRepetiteurDTO;
import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.model.Matiere_Repetiteur;
import cm.darttc.damocles.model.Repetiteur;
import cm.darttc.damocles.model.Repetition;
import cm.darttc.damocles.model.Utilisateur;
import cm.darttc.damocles.services.MatiereRepetiteurService;
import cm.darttc.damocles.services.Matiere_Repetition_ApprenantService;
import cm.darttc.damocles.services.RepetiteurService;
import cm.darttc.damocles.services.UtilisateurService;

/**
 * 
 */
@Path("/repetiteurs")
public class RepetiteurEndpoint {
	
	@EJB
	RepetiteurService repetiteurService;
	@EJB
	private UtilisateurService utilisateurService;
	@EJB
	private MatiereRepetiteurService matiereRepetiteurService;
	@EJB
	private Matiere_Repetition_ApprenantService matiere_Repetition_ApprenantService;

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(InscriptionRepetiteurDTO entity) {
		
		Repetiteur repetiteur = new Repetiteur();
		repetiteur.setNom(entity.getNom());
		repetiteur.setPrenom(entity.getPrenom());
		repetiteur.setTelephone(entity.getTelephone());
		repetiteur.setQuartier(entity.getQuartier());
		repetiteur.setNumero_rue(entity.getNumero_rue());
		repetiteur.setNiveau_scolaire(entity.getNiveau_scolaire());
		repetiteur.setLogin(entity.getLogin());
		repetiteur.setPassword(entity.getPassword());
		
		repetiteurService.create(repetiteur);
		
		/* creation utilisateur */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId_personne(repetiteur.getId());
		utilisateur.setType_utilisateur("repetiteur");
		utilisateurService.create(utilisateur);
		
		/* creation Matiere_Repetiteur*/
		Matiere[] tableauMatieres = entity.getTableauMatieres();
		int taille = entity.getTableauMatieres().length;
		
		for(int i=0; i< taille; i++){
			
			Matiere_Repetiteur matiere_Repetiteur = new Matiere_Repetiteur();
			matiere_Repetiteur.setMatiere(tableauMatieres[i]);
			matiere_Repetiteur.setRepetiteur(repetiteur);
			matiereRepetiteurService.create(matiere_Repetiteur);
		}
		
		return Response.created(
				UriBuilder.fromResource(RepetiteurEndpoint.class)
						.path(String.valueOf(repetiteur.getId())).build()).build();
	}

	@DELETE
	@Path("/delete")
	public Response deleteById(Long id) {
		Repetiteur entity = repetiteurService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		repetiteurService.deleteById(id);
		return Response.noContent().build();
	}

	@POST
	@Path("/get")
	@Produces("application/json")
	public Response findById(Long id) {
		Repetiteur entity = repetiteurService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<Repetiteur> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		
		
		final List<Repetiteur> results = repetiteurService.findRepetiteurs();
		return results;
	}

	@PUT
	@Path("/update")
	@Consumes("application/json")
	public Response update(Repetiteur entity) {
		
		Long id = entity.getId();
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (repetiteurService.findById(id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = repetiteurService.update(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
	
	@POST
	@Path("/addRepetition")
	@Produces("application/json")
	public Response addRepetition(Repetition repetition) {
		
		//on recupere la liste des matieres de la répétition A
		List<Matiere> matieres_repetition = matiere_Repetition_ApprenantService.matieres_repetition(repetition.getId());
				
		//on recupere la liste de tous les repetiteurs specialisés dans au moins une matiere de la repetition AB
		List<Repetiteur> liste_repetiteurs_specialise = repetiteurService.repetiteurs(matieres_repetition,repetition);
		
		List<Repetiteur> repetiteurs_validés = repetiteurService.matieres_repetiteur(liste_repetiteurs_specialise,matieres_repetition,repetition);
		
		return Response.ok(repetiteurs_validés).build();
	}
}
