package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import cm.darttc.damocles.model.Matiere_Repetition_Apprenant;
import cm.darttc.damocles.services.Matiere_Repetition_ApprenantService;

/**
 * 
 */
@Path("/matiere_repetition_apprenants")
public class Matiere_Repetition_ApprenantEndpoint {

	
	@EJB
	private Matiere_Repetition_ApprenantService matiere_Repetition_ApprenantService;
	@POST
	@Consumes("application/json")
	public Response create(Matiere_Repetition_Apprenant entity) {
		matiere_Repetition_ApprenantService.create(entity);
		return Response.created(
				UriBuilder
						.fromResource(
								Matiere_Repetition_ApprenantEndpoint.class)
						.path(String.valueOf(entity.getId())).build()).build();
	}

	@DELETE
	@Path("/{id:[0-9][0-9]*}")
	public Response deleteById(@PathParam("id") Long id) {
		Matiere_Repetition_Apprenant entity = matiere_Repetition_ApprenantService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		matiere_Repetition_ApprenantService.deleteById(id);
		return Response.noContent().build();
	}

	@GET
	@Path("/{id:[0-9][0-9]*}")
	@Produces("application/json")
	public Response findById(@PathParam("id") Long id) {
		Matiere_Repetition_Apprenant entity = matiere_Repetition_ApprenantService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<Matiere_Repetition_Apprenant> listAll(
			@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		
		final List<Matiere_Repetition_Apprenant> results = matiere_Repetition_ApprenantService.findMatiere_Repetition_Apprenant();
		return results;
	}

	@PUT
	@Path("/{id:[0-9][0-9]*}")
	@Consumes("application/json")
	public Response update(@PathParam("id") Long id,
			Matiere_Repetition_Apprenant entity) {
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (matiere_Repetition_ApprenantService.findById(id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = matiere_Repetition_ApprenantService.update(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
}
