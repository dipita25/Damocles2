package cm.darttc.damocles.rest;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.OptimisticLockException;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import cm.darttc.damocles.DTO.InscriptionParentDTO;
import cm.darttc.damocles.enumeration.Etat;
import cm.darttc.damocles.model.Apprenant;
import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.model.Matiere_Repetition_Apprenant;
import cm.darttc.damocles.model.Parent;
import cm.darttc.damocles.model.Repetiteur;
import cm.darttc.damocles.model.Repetition;
import cm.darttc.damocles.model.Repetition_Apprenant;
import cm.darttc.damocles.model.Repetition_Repetiteur;
import cm.darttc.damocles.model.Utilisateur;
import cm.darttc.damocles.services.ApprenantService;
import cm.darttc.damocles.services.Matiere_RepetitionService;
import cm.darttc.damocles.services.Matiere_Repetition_ApprenantService;
import cm.darttc.damocles.services.ParentService;
import cm.darttc.damocles.services.RepetiteurService;
import cm.darttc.damocles.services.RepetitionApprenantService;
import cm.darttc.damocles.services.RepetitionRepetiteurService;
import cm.darttc.damocles.services.RepetitionService;
import cm.darttc.damocles.services.UtilisateurService;

/**
 * 
 */
@Path("/parents")
@TransactionManagement(TransactionManagementType.BEAN)

public class ParentEndpoint {
	
	@EJB
	private ParentService parentService;
	@EJB
	private RepetitionRepetiteurService repetitionRepetiteurService;
	@EJB
	private RepetiteurService repetiteurService;
	@EJB
	private RepetitionService repetitionService;
	@EJB
	private UtilisateurService utilisateurService;
	@EJB
	private ApprenantService apprenantService;
	@EJB
	private RepetitionApprenantService repetitionApprenantService;
	@EJB
	private Matiere_RepetitionService  matiere_RepetitionService;
	@EJB
	private Matiere_Repetition_ApprenantService matiere_Repetition_ApprenantService;

	@POST
	@Path("/create")
	@Consumes("application/json")
	public Response create(InscriptionParentDTO entity) {

		/* creation parent */
		Parent parent = new Parent();
		parent.setNom(entity.getNom_parent());
		parent.setLogin(entity.getLogin_parent());
		parent.setNumero_rue(entity.getNumero_rue_parent());
		parent.setPrenom(entity.getPrenom_parent());
		parent.setPassword(entity.getPassword_parent());
		parent.setQuartier(entity.getQuartier_parent());
		parent.setTelephone(entity.getTelephone_parent());
		parentService.create(parent);
		
		/* creation utilisateur */
		Utilisateur utilisateur = new Utilisateur();
		utilisateur.setId_personne(parent.getId());
		utilisateur.setType_utilisateur("parent");
		utilisateurService.create(utilisateur);
		
		/* creation repetition*/
		Repetition repetition = new Repetition();
		repetition.setNiveau_scolaire_repetiteur(entity.getNiveau_scolaire_repetiteur_repetition());
		repetition.setStatut(Etat.attente);
		repetition.setParent(parent);
		java.util.Date actuelle = new java.util.Date();
		repetition.setDate_souscription(actuelle);
		repetitionService.create(repetition);
		
		/* creation apprenant */
		
		Apprenant[] tableauApprenants = entity.getApprenants();
		/*int tailleApprenant = entity.getApprenants().length;*/
		List<Matiere[]> matiereApprenant = entity.getTableauMatieres_apprenant();
		
		for(int i=0;i < matiereApprenant.size(); i++ ){
			Apprenant apprenant = new Apprenant();
			apprenant.setNom(tableauApprenants[i].getNom());
			apprenant.setPrenom(tableauApprenants[i].getPrenom());
			apprenant.setClasse(tableauApprenants[i].getClasse());
			apprenant.setEtablissement(tableauApprenants[i].getEtablissement());
			apprenant.setAge(tableauApprenants[i].getAge());
			apprenant.setParent(parent);
			apprenantService.create(apprenant);
		
			/* creation Apprenant_Repetition*/
			Repetition_Apprenant apprenant_Repetition = new Repetition_Apprenant();
			apprenant_Repetition.setApprenant(apprenant);
			apprenant_Repetition.setRepetition(repetition);
			repetitionApprenantService.create(apprenant_Repetition);
			
			/* creation matiere_repetition_apprenant */
			
			
			
				for (int t=0; t< matiereApprenant.get(i).length; t++){
					Matiere_Repetition_Apprenant matiere_Repetition_Apprenant = new Matiere_Repetition_Apprenant();
					matiere_Repetition_Apprenant.setMatiere(matiereApprenant.get(i)[t]);
					matiere_Repetition_Apprenant.setRepetition_apprenant(apprenant_Repetition);
					matiere_Repetition_ApprenantService.create(matiere_Repetition_Apprenant);
				}
			
			
		}
			//on recupere la liste des matieres de la répétition A
		//List<Matiere> matieres_repetition = matiere_Repetition_ApprenantService.matieres_repetition(repetition.getId());
		
			//on recupere la liste de tous les repetiteurs specialisés dans au moins une matiere de la repetition AB
		//List<Repetiteur> liste_repetiteurs_specialise = repetiteurService.repetiteurs(matieres_repetition,repetition);
		
		//List<Repetiteur> repetiteurs_validés = repetiteurService.matieres_repetiteur(liste_repetiteurs_specialise,matieres_repetition,repetition);
		//fin attribution repetiteurs
		/*return Response.created(
				UriBuilder.fromResource(ParentEndpoint.class)
						.path(String.valueOf(parent.getId())).build()).build();*/
		
									return Response.ok(repetition).build();
	}

	@DELETE
	@Path("/delete")
	public Response deleteById(Long id) {
		Parent entity = parentService.findById(id);
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		parentService.deleteById(id);
		return Response.noContent().build();
	}

	@POST
	@Path("/get")
	@Produces("application/json")
	public Response findById(Long id) {
		
		Parent entity = parentService.findById(id);
		
		if (entity == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		return Response.ok(entity).build();
	}

	@GET
	@Produces("application/json")
	public List<Parent> listAll(@QueryParam("start") Integer startPosition,
			@QueryParam("max") Integer maxResult) {
		
		List<Parent> results = parentService.findParents();
		return results;
	}

	@PUT
	@Path("/update")
	@Consumes("application/json")
	public Response update(Parent entity) {
		
		/*Parent entity = new Parent();*/
		Long id = entity.getId();
		
		/*entity.setVersion(element.getVersion());
		entity.setId(element.getId());
		entity.setLogin(element.getLogin());
		entity.setNom(element.getNom());
		entity.setNumero_rue(element.getNumero_rue());
		entity.setPassword(element.getPassword());
		entity.setPrenom(element.getPrenom());
		entity.setQuartier(element.getQuartier());
		entity.setTelephone(element.getTelephone());
		
		Parent parent = parentService.findById(element.getParent().getId());
		entity.setParent(parent);*/
		
		if (entity == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (id == null) {
			return Response.status(Status.BAD_REQUEST).build();
		}
		if (!id.equals(entity.getId())) {
			return Response.status(Status.CONFLICT).entity(entity).build();
		}
		if (parentService.findById(id) == null) {
			return Response.status(Status.NOT_FOUND).build();
		}
		try {
			entity = parentService.update(entity);
		} catch (OptimisticLockException e) {
			return Response.status(Response.Status.CONFLICT)
					.entity(e.getEntity()).build();
		}

		return Response.noContent().build();
	}
}
