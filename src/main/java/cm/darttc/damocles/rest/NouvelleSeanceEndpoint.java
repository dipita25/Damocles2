package cm.darttc.damocles.rest;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.SeanceDTO;
import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.services.NouvelleSeanceService;

@Path("/newSeance")
public class NouvelleSeanceEndpoint {

	@EJB
	private NouvelleSeanceService nouvelleSeanceService;
	
	@POST
	@Produces("application/json")
	public Response enregistrer(SeanceDTO seanceDTO) {
		
		try{
			Seance seance = new Seance();
			seance = nouvelleSeanceService.enregistrer(seanceDTO);
			return Response.ok(seance).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
		
	}

}
