package cm.darttc.damocles.rest;

import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.UtilisateurDTO;
import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.services.FeedbackRepetiteurService;

@Path("/feedbackRepetiteur")
public class FeedbackRepetiteurEndpoint {


	@EJB
	private FeedbackRepetiteurService feedbackRepetiteurService;

	
	@POST
	@Produces("application/json")
	public Response enregistrer(UtilisateurDTO utilisateurDTO) {
		
		try{
			List<Seance> seances = feedbackRepetiteurService.cherche(utilisateurDTO);
			return Response.ok(seances).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
	}


}
