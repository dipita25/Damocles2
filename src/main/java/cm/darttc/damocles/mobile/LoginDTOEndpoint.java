package cm.darttc.damocles.mobile;

import javax.ejb.EJB;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.LoginDTO;
import cm.darttc.damocles.model.Utilisateur;
import cm.darttc.damocles.services.mobile.LoginDTOServiceRepetiteur;


@Path("/loginRepetiteur")
public class LoginDTOEndpoint {

	@EJB
	private LoginDTOServiceRepetiteur loginService;
	
	@POST
	@Produces("application/json")
	public Response find(LoginDTO loginDto) {
		
		try{
			Utilisateur utilisateur = loginService.authenticate(loginDto);
			return Response.ok(utilisateur).build();
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}
	}
	
}
