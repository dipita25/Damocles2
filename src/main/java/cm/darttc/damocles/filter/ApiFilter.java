package cm.darttc.damocles.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class ApiFilter implements ContainerResponseFilter,  Filter {

	
	@Override
	public void filter(ContainerRequestContext arg0, ContainerResponseContext arg1) throws IOException {
		arg1.getHeaders().add("Access-Control-Allow-Origin", "*");
		arg1.getHeaders().add("Access-Control-Allow-Headers", "origin, content-type, accept, authorization");
		arg1.getHeaders().add("Access-Control-Allow-Credentials", "true");
		arg1.getHeaders().add("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS, HEAD");
		arg1.getHeaders().add("Access-Control-Max-Age", "1209600");
		System.out.println("filtrer");
	  }

	@Override
	public void destroy() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void doFilter(ServletRequest arg0, ServletResponse arg1, FilterChain arg2)
			throws IOException, ServletException {
		// TODO Auto-generated method stub
		System.out.println("le filtre est activé");
		
		
		
		//cette ligne permet de laisser continuer la requete
		arg2.doFilter(arg0,arg1);
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
