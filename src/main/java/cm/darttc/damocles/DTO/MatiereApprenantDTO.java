package cm.darttc.damocles.DTO;

import cm.darttc.damocles.model.Matiere;

public class MatiereApprenantDTO {
	
	private Long id;
	
	private Matiere[] matieres;

	
	public Matiere[] getMatieres() {
		return matieres;
	}

	public void setMatieres(Matiere[] matieres) {
		this.matieres = matieres;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	

}
