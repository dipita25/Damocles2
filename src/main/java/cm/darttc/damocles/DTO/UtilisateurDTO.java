package cm.darttc.damocles.DTO;

public class UtilisateurDTO {

	private Long id_personne;

	private String type_utilisateur;

	public Long getId_personne() {
		return id_personne;
	}

	public void setId_personne(Long id_personne) {
		this.id_personne = id_personne;
	}

	public String getType_utilisateur() {
		return type_utilisateur;
	}

	public void setType_utilisateur(String type_utilisateur) {
		this.type_utilisateur = type_utilisateur;
	}
	
}
