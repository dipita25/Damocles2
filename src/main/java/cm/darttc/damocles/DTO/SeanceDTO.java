package cm.darttc.damocles.DTO;

import java.util.Date;

import cm.darttc.damocles.model.Repetition;

public class SeanceDTO {

	private Long id_apprenant;
	
	private String lecon;

	private String lieu;

	private Date date_seance;

	private Date heure_debut_seance;

	private Date heure_fin_seance;

	private Repetition repetition;

	public Long getId_apprenant() {
		return id_apprenant;
	}

	public void setId_apprenant(Long id_apprenant) {
		this.id_apprenant = id_apprenant;
	}

	public String getLecon() {
		return lecon;
	}

	public void setLecon(String lecon) {
		this.lecon = lecon;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public Date getDate_seance() {
		return date_seance;
	}

	public void setDate_seance(Date date_seance) {
		this.date_seance = date_seance;
	}

	public Date getHeure_debut_seance() {
		return heure_debut_seance;
	}

	public void setHeure_debut_seance(Date heure_debut_seance) {
		this.heure_debut_seance = heure_debut_seance;
	}

	public Date getHeure_fin_seance() {
		return heure_fin_seance;
	}

	public void setHeure_fin_seance(Date heure_fin_seance) {
		this.heure_fin_seance = heure_fin_seance;
	}

	public Repetition getRepetition() {
		return repetition;
	}

	public void setRepetition(Repetition repetition) {
		this.repetition = repetition;
	}
	
	
}
