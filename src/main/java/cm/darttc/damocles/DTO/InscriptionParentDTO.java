package cm.darttc.damocles.DTO;

import java.util.List;

import cm.darttc.damocles.enumeration.Etat;
import cm.darttc.damocles.model.Apprenant;
import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.model.Parent;
import cm.darttc.damocles.model.Repetition;

public class InscriptionParentDTO {

	private Apprenant[] apprenants;
	private  Apprenant apprenant;
	
	private String nom_apprenant;

	private String prenom_apprenant;

	private String classe_apprenant;

	private int age_apprenant;

	private String etablissement_apprenant;

	private Parent parent;
	
	private String nom_parent;

	private String prenom_parent;

	private int telephone_parent;

	private String quartier_parent;

	private int numero_rue_parent;

	private String login_parent;

	private String password_parent;
	
	private Long id_personne_utilisateur;

	private String type_utilisateur_utilisateur;
	
	private Etat statut_repetition;
	
	private String niveau_scolaire_repetiteur_repetition;
	
	private Apprenant apprenant_repetition_apprenant;

	private Repetition repetition_repetition_apprenant;
	
	private Matiere[] tableauMatieres;
	
	private List<Matiere[]> tableauMatieres_apprenant;
	
	

	public List<Matiere[]> getTableauMatieres_apprenant() {
		return tableauMatieres_apprenant;
	}

	public void setTableauMatieres_apprenant(List<Matiere[]> tableauMatieres_apprenant) {
		this.tableauMatieres_apprenant = tableauMatieres_apprenant;
	}

	public Apprenant getApprenant() {
		return apprenant;
	}

	public void setApprenant(Apprenant apprenant) {
		this.apprenant = apprenant;
	}

	public Apprenant[] getApprenants() {
		return apprenants;
	}

	public void setApprenants(Apprenant[] apprenants) {
		this.apprenants = apprenants;
	}

	public String getNom_apprenant() {
		return nom_apprenant;
	}

	public void setNom_apprenant(String nom_apprenant) {
		this.nom_apprenant = nom_apprenant;
	}

	public String getPrenom_apprenant() {
		return prenom_apprenant;
	}

	public void setPrenom_apprenant(String prenom_apprenant) {
		this.prenom_apprenant = prenom_apprenant;
	}

	public String getClasse_apprenant() {
		return classe_apprenant;
	}

	public void setClasse_apprenant(String classe_apprenant) {
		this.classe_apprenant = classe_apprenant;
	}

	public int getAge_apprenant() {
		return age_apprenant;
	}

	public void setAge_apprenant(int age_apprenant) {
		this.age_apprenant = age_apprenant;
	}

	public String getEtablissement_apprenant() {
		return etablissement_apprenant;
	}

	public void setEtablissement_apprenant(String etablissement_apprenant) {
		this.etablissement_apprenant = etablissement_apprenant;
	}

	public Parent getParent() {
		return parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public String getNom_parent() {
		return nom_parent;
	}

	public void setNom_parent(String nom_parent) {
		this.nom_parent = nom_parent;
	}

	public String getPrenom_parent() {
		return prenom_parent;
	}

	public void setPrenom_parent(String prenom_parent) {
		this.prenom_parent = prenom_parent;
	}

	public int getTelephone_parent() {
		return telephone_parent;
	}

	public void setTelephone_parent(int telephone_parent) {
		this.telephone_parent = telephone_parent;
	}

	public String getQuartier_parent() {
		return quartier_parent;
	}

	public void setQuartier_parent(String quartier_parent) {
		this.quartier_parent = quartier_parent;
	}

	public Matiere[] getTableauMatieres() {
		return tableauMatieres;
	}

	public void setTableauMatieres(Matiere[] tableauMatieres) {
		this.tableauMatieres = tableauMatieres;
	}

	public int getNumero_rue_parent() {
		return numero_rue_parent;
	}

	public void setNumero_rue_parent(int numero_rue_parent) {
		this.numero_rue_parent = numero_rue_parent;
	}

	public String getLogin_parent() {
		return login_parent;
	}

	public void setLogin_parent(String login_parent) {
		this.login_parent = login_parent;
	}

	public String getPassword_parent() {
		return password_parent;
	}

	public void setPassword_parent(String password_parent) {
		this.password_parent = password_parent;
	}

	public Long getId_personne_utilisateur() {
		return id_personne_utilisateur;
	}

	public void setId_personne_utilisateur(Long id_personne_utilisateur) {
		this.id_personne_utilisateur = id_personne_utilisateur;
	}

	public String getType_utilisateur_utilisateur() {
		return type_utilisateur_utilisateur;
	}

	public void setType_utilisateur_utilisateur(String type_utilisateur_utilisateur) {
		this.type_utilisateur_utilisateur = type_utilisateur_utilisateur;
	}

	public Etat getStatut_repetition() {
		return statut_repetition;
	}

	public void setStatut_repetition(Etat statut_repetition) {
		this.statut_repetition = statut_repetition;
	}

	public String getNiveau_scolaire_repetiteur_repetition() {
		return niveau_scolaire_repetiteur_repetition;
	}

	public void setNiveau_scolaire_repetiteur_repetition(String niveau_scolaire_repetiteur_repetition) {
		this.niveau_scolaire_repetiteur_repetition = niveau_scolaire_repetiteur_repetition;
	}

	public Apprenant getApprenant_repetition_apprenant() {
		return apprenant_repetition_apprenant;
	}

	public void setApprenant_repetition_apprenant(Apprenant apprenant_repetition_apprenant) {
		this.apprenant_repetition_apprenant = apprenant_repetition_apprenant;
	}

	public Repetition getRepetition_repetition_apprenant() {
		return repetition_repetition_apprenant;
	}

	public void setRepetition_repetition_apprenant(Repetition repetition_repetition_apprenant) {
		this.repetition_repetition_apprenant = repetition_repetition_apprenant;
	}

	
}
