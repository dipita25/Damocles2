package cm.darttc.damocles.DTO;

import cm.darttc.damocles.model.Matiere;

public class InscriptionRepetiteurDTO {

	private String nom;

	private String prenom;

	private String niveau_scolaire;

	private int telephone;

	private String quartier;

	private int numero_rue;

	private String login;

	private String password;
	
	private Matiere[] tableauMatieres;

	public Matiere[] getTableauMatieres() {
		return tableauMatieres;
	}

	public void setTableauMatieres(Matiere[] tableauMatieres) {
		this.tableauMatieres = tableauMatieres;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNiveau_scolaire() {
		return niveau_scolaire;
	}

	public void setNiveau_scolaire(String niveau_scolaire) {
		this.niveau_scolaire = niveau_scolaire;
	}

	public int getTelephone() {
		return telephone;
	}

	public void setTelephone(int telephone) {
		this.telephone = telephone;
	}

	public String getQuartier() {
		return quartier;
	}

	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}

	public int getNumero_rue() {
		return numero_rue;
	}

	public void setNumero_rue(int numero_rue) {
		this.numero_rue = numero_rue;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
