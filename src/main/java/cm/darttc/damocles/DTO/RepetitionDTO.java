package cm.darttc.damocles.DTO;

import cm.darttc.damocles.enumeration.Etat;
import cm.darttc.damocles.enumeration.Niveau;
import cm.darttc.damocles.model.Parent;

public class RepetitionDTO {

		private Etat statut;
		
		private Niveau niveau_scolaire;

		private Parent parent;
		
		public Niveau getNiveau_scolaire() {
			return niveau_scolaire;
		}

		public void setNiveau_scolaire(Niveau niveau_scolaire) {
			this.niveau_scolaire = niveau_scolaire;
		}

		public Etat getStatut() {
			return statut;
		}

		public void setStatut(Etat statut) {
			this.statut = statut;
		}

		public Parent getParent() {
			return this.parent;
		}

		public void setParent(final Parent parent) {
			this.parent = parent;
		}
	
}
