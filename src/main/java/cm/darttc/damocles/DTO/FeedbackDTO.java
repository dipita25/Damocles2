package cm.darttc.damocles.DTO;

import java.util.Date;

import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.model.Utilisateur;

public class FeedbackDTO {

	
	private Date heure_debut;

	private Date heure_fin;

	private Date heure_arrivee;

	private Date heure_depart;

	private String appreciation;

	private Seance seance;

	private Utilisateur auteur;
	
	private Long id_auteur;
	
	private Long id_seance;
	
	

	

	public Long getId_auteur() {
		return id_auteur;
	}

	public void setId_auteur(Long id_auteur) {
		this.id_auteur = id_auteur;
	}

	public Long getId_seance() {
		return id_seance;
	}

	public void setId_seance(Long id_seance) {
		this.id_seance = id_seance;
	}

	public Date getHeure_debut() {
		return heure_debut;
	}

	public void setHeure_debut(Date heure_debut) {
		this.heure_debut = heure_debut;
	}

	public Date getHeure_fin() {
		return heure_fin;
	}

	public void setHeure_fin(Date heure_fin) {
		this.heure_fin = heure_fin;
	}

	public Date getHeure_arrivee() {
		return heure_arrivee;
	}

	public void setHeure_arrivee(Date heure_arrivee) {
		this.heure_arrivee = heure_arrivee;
	}

	public Date getHeure_depart() {
		return heure_depart;
	}

	public void setHeure_depart(Date heure_depart) {
		this.heure_depart = heure_depart;
	}

	public String getAppreciation() {
		return appreciation;
	}

	public void setAppreciation(String appreciation) {
		this.appreciation = appreciation;
	}

	public Seance getSeance() {
		return seance;
	}

	public void setSeance(Seance seance) {
		this.seance = seance;
	}

	public Utilisateur getAuteur() {
		return auteur;
	}

	public void setAuteur(Utilisateur auteur) {
		this.auteur = auteur;
	}

	
}
