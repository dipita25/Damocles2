package cm.darttc.damocles.DTO;

import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.model.Utilisateur;

public class FeedbackMobileDTO {
	
	private int heure_debut;
	private int heure_debut_minute;

	private int heure_fin;
	private int heure_fin_minute;

	private int heure_arrivee;
	private int heure_arrivee_minute;

	private int heure_depart;
	private int heure_depart_minute;

	private String appreciation;
	
	private Utilisateur auteur;
	
	private Long id_auteur;
	
	private Long id_seance;
	
	

	

	public Long getId_auteur() {
		return id_auteur;
	}

	public void setId_auteur(Long id_auteur) {
		this.id_auteur = id_auteur;
	}

	public Long getId_seance() {
		return id_seance;
	}

	public void setId_seance(Long id_seance) {
		this.id_seance = id_seance;
	}

	public int getHeure_debut() {
		return heure_debut;
	}

	public void setHeure_debut(int heure_debut) {
		this.heure_debut = heure_debut;
	}

	public int getHeure_fin() {
		return heure_fin;
	}

	public void setHeure_fin(int heure_fin) {
		this.heure_fin = heure_fin;
	}

	public int getHeure_arrivee() {
		return heure_arrivee;
	}

	public void setHeure_arrivee(int heure_arrivee) {
		this.heure_arrivee = heure_arrivee;
	}

	public int getHeure_depart() {
		return heure_depart;
	}

	public void setHeure_depart(int heure_depart) {
		this.heure_depart = heure_depart;
	}

	public int getHeure_debut_minute() {
		return heure_debut_minute;
	}

	public void setHeure_debut_minute(int heure_debut_minute) {
		this.heure_debut_minute = heure_debut_minute;
	}

	public int getHeure_fin_minute() {
		return heure_fin_minute;
	}

	public void setHeure_fin_minute(int heure_fin_minute) {
		this.heure_fin_minute = heure_fin_minute;
	}

	public int getHeure_arrivee_minute() {
		return heure_arrivee_minute;
	}

	public void setHeure_arrivee_minute(int heure_arrivee_minute) {
		this.heure_arrivee_minute = heure_arrivee_minute;
	}

	public int getHeure_depart_minute() {
		return heure_depart_minute;
	}

	public void setHeure_depart_minute(int heure_depart_minute) {
		this.heure_depart_minute = heure_depart_minute;
	}

	public String getAppreciation() {
		return appreciation;
	}

	public void setAppreciation(String appreciation) {
		this.appreciation = appreciation;
	}

	public Utilisateur getAuteur() {
		return auteur;
	}

	public void setAuteur(Utilisateur auteur) {
		this.auteur = auteur;
	}

}
