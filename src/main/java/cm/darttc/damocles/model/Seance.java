package cm.darttc.damocles.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Seance implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column
	private String lecon;

	@Column
	private String lieu;

	@Column
	@Temporal(TemporalType.DATE)
	private Date date_seance;

	@Column
	@Temporal(TemporalType.TIME)
	private Date heure_debut_seance;

	@Column
	@Temporal(TemporalType.TIME)
	private Date heure_fin_seance;

	@ManyToOne
	private Repetition repetition;
	
	private String statut;
	

	public String getStatut() {
		return statut;
	}

	public void setStatut(String statut) {
		this.statut = statut;
	}

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Seance)) {
			return false;
		}
		Seance other = (Seance) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public String getLecon() {
		return lecon;
	}

	public void setLecon(String lecon) {
		this.lecon = lecon;
	}

	public String getLieu() {
		return lieu;
	}

	public void setLieu(String lieu) {
		this.lieu = lieu;
	}

	public Date getDate_seance() {
		return date_seance;
	}

	public void setDate_seance(Date date_seance) {
		this.date_seance = date_seance;
	}

	public Date getHeure_debut_seance() {
		return heure_debut_seance;
	}

	public void setHeure_debut_seance(Date heure_debut_seance) {
		this.heure_debut_seance = heure_debut_seance;
	}

	

	public Date getHeure_fin_seance() {
		return heure_fin_seance;
	}

	public void setHeure_fin_seance(Date heure_fin_seance) {
		this.heure_fin_seance = heure_fin_seance;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (lecon != null && !lecon.trim().isEmpty())
			result += "lecon: " + lecon;
		if (lieu != null && !lieu.trim().isEmpty())
			result += ", lieu: " + lieu;
		return result;
	}

	public Repetition getRepetition() {
		return this.repetition;
	}

	public void setRepetition(final Repetition repetition) {
		this.repetition = repetition;
	}
}