package cm.darttc.damocles.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

import cm.darttc.damocles.enumeration.Etat;

@Entity
@XmlRootElement
public class Repetition implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Enumerated
	private Etat statut;
	
	@Column
	private String niveau_scolaire_repetiteur;

	@ManyToOne
	private Parent parent;
	
	private java.util.Date date_souscription;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Repetition)) {
			return false;
		}
		Repetition other = (Repetition) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Etat getStatut() {
		return statut;
	}

	public void setStatut(Etat statut) {
		this.statut = statut;
	}

	
	public String getNiveau_scolaire_repetiteur() {
		return niveau_scolaire_repetiteur;
	}

	public void setNiveau_scolaire_repetiteur(String niveau_scolaire_repetiteur) {
		this.niveau_scolaire_repetiteur = niveau_scolaire_repetiteur;
	}

	public Parent getParent() {
		return this.parent;
	}

	public void setParent(Parent parent) {
		this.parent = parent;
	}

	public java.util.Date getDate_souscription() {
		return date_souscription;
	}

	public void setDate_souscription(java.util.Date date_souscription) {
		this.date_souscription = date_souscription;
	}
	
	
}