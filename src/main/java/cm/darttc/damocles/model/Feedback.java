package cm.darttc.damocles.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Feedback implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@Column
	@Temporal(TemporalType.TIME)
	private Date heure_debut;

	@Column
	@Temporal(TemporalType.TIME)
	private Date heure_fin;

	@Column
	@Temporal(TemporalType.TIME)
	private Date heure_arrivee;

	@Column
	@Temporal(TemporalType.TIME)
	private Date heure_depart;

	@Column
	private String appreciation;

	@OneToOne
	private Seance seance;

	@OneToOne
	private Utilisateur auteur;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Feedback)) {
			return false;
		}
		Feedback other = (Feedback) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Date getHeure_debut() {
		return heure_debut;
	}

	public void setHeure_debut(Date heure_debut) {
		this.heure_debut = heure_debut;
	}

	public Date getHeure_fin() {
		return heure_fin;
	}

	public void setHeure_fin(Date heure_fin) {
		this.heure_fin = heure_fin;
	}

	public Date getHeure_arrivee() {
		return heure_arrivee;
	}

	public void setHeure_arrivee(Date heure_arrivee) {
		this.heure_arrivee = heure_arrivee;
	}

	public Date getHeure_depart() {
		return heure_depart;
	}

	public void setHeure_depart(Date heure_depart) {
		this.heure_depart = heure_depart;
	}

	public String getAppreciation() {
		return appreciation;
	}

	public void setAppreciation(String appreciation) {
		this.appreciation = appreciation;
	}

	public Seance getSeance() {
		return seance;
	}

	public void setSeance(Seance seance) {
		this.seance = seance;
	}

	public Utilisateur getAuteur() {
		return auteur;
	}

	public void setAuteur(Utilisateur auteur) {
		this.auteur = auteur;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (appreciation != null && !appreciation.trim().isEmpty())
			result += "appreciation: " + appreciation;
		return result;
	}
}