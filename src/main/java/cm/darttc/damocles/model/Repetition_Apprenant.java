package cm.darttc.damocles.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import cm.darttc.damocles.model.Apprenant;
import javax.persistence.ManyToOne;
import cm.darttc.damocles.model.Repetition;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Repetition_Apprenant implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne
	private Apprenant apprenant;

	@ManyToOne
	private Repetition repetition;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (id != null)
			result += "id: " + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Repetition_Apprenant)) {
			return false;
		}
		Repetition_Apprenant other = (Repetition_Apprenant) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Apprenant getApprenant() {
		return this.apprenant;
	}

	public void setApprenant(final Apprenant apprenant) {
		this.apprenant = apprenant;
	}

	public Repetition getRepetition() {
		return this.repetition;
	}

	public void setRepetition(final Repetition repetition) {
		this.repetition = repetition;
	}
}