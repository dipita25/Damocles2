package cm.darttc.damocles.model;

import javax.persistence.Entity;
import java.io.Serializable;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Column;
import javax.persistence.Version;
import cm.darttc.damocles.model.Repetiteur;
import javax.persistence.ManyToOne;
import cm.darttc.damocles.model.Matiere;
import javax.xml.bind.annotation.XmlRootElement;

@Entity
@XmlRootElement
public class Matiere_Repetiteur implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", updatable = false, nullable = false)
	private Long id;
	@Version
	@Column(name = "version")
	private int version;

	@ManyToOne
	private Repetiteur repetiteur;

	@ManyToOne
	private Matiere matiere;

	public Long getId() {
		return this.id;
	}

	public void setId(final Long id) {
		this.id = id;
	}

	public int getVersion() {
		return this.version;
	}

	public void setVersion(final int version) {
		this.version = version;
	}

	@Override
	public String toString() {
		String result = getClass().getSimpleName() + " ";
		if (id != null)
			result += "id: " + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Matiere_Repetiteur)) {
			return false;
		}
		Matiere_Repetiteur other = (Matiere_Repetiteur) obj;
		if (id != null) {
			if (!id.equals(other.id)) {
				return false;
			}
		}
		return true;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	public Repetiteur getRepetiteur() {
		return this.repetiteur;
	}

	public void setRepetiteur(final Repetiteur repetiteur) {
		this.repetiteur = repetiteur;
	}

	public Matiere getMatiere() {
		return this.matiere;
	}

	public void setMatiere(final Matiere matiere) {
		this.matiere = matiere;
	}
}