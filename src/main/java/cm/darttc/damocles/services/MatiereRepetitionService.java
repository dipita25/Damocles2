package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Matiere;

@Stateless
public class MatiereRepetitionService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	
	public List<Matiere> mesMatieres(ApprenantDTO apprenantDTO){
		
		
		TypedQuery<Matiere> mesMatieresQuery = em.createQuery("SELECT DISTINCT m FROM Matiere m WHERE m.id IN (SELECT DISTINCT mra.matiere.id FROM Matiere_Repetition_Apprenant mra "
				+ "WHERE mra.repetition_apprenant.id IN (SELECT DISTINCT ra.id FROM Repetition_Apprenant ra WHERE ra.apprenant.id = :entityIds))",Matiere.class);
		
		mesMatieresQuery.setParameter("entityIds", apprenantDTO.getId());
		
		List<Matiere> matieres = mesMatieresQuery.getResultList();
		
		
		return matieres;
		
	}

}
