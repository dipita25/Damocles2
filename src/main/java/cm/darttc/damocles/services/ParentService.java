package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Parent;

@Stateless
public class ParentService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Parent create(Parent parent) {
		em.persist(parent);
		return parent;
	}
	
	public Parent findById(Long id)
	{
		Parent entity = em.find(Parent.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Parent entity = em.find(Parent.class,id);
		em.remove(entity);
		
	}
	
	public Parent update(Parent parent)
	{
		em.merge(parent);
		return parent;
	}
	
	public List<Parent> findParents (){
		TypedQuery<Parent> ParentQuery = em.createQuery("SELECT DISTINCT p FROM Parent p ",Parent.class);
		List<Parent> resultList = ParentQuery.getResultList();
		return resultList;
	}

}
