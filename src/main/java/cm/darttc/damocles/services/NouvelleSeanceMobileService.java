package cm.darttc.damocles.services;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.SeanceMobileDTO;
import cm.darttc.damocles.model.Repetition;
import cm.darttc.damocles.model.Seance;

@Stateless
public class NouvelleSeanceMobileService {
	
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Seance enregistrer(SeanceMobileDTO seanceMobileDTO){
		
		TypedQuery<Repetition> findQuery = em
				.createQuery(
						"SELECT DISTINCT r FROM Repetition r WHERE r.id IN (SELECT  ra.repetition.id FROM Repetition_Apprenant ra "
						+ "WHERE ra.apprenant.id = :entityId)",
						Repetition.class);
		findQuery.setParameter("entityId", seanceMobileDTO.getId_apprenant());
		
		Repetition repetition = findQuery.getSingleResult();
		
		seanceMobileDTO.setRepetition(repetition);
		
		Date Heure_debut_seance = new Date();
		Heure_debut_seance.setHours(seanceMobileDTO.getHeure_debut_seance());
		Heure_debut_seance.setMinutes(seanceMobileDTO.getHeure_debut_seance_minute());
		
		Date Heure_fin_seance = new Date();
		Heure_fin_seance.setHours(seanceMobileDTO.getHeure_fin_seance());
		Heure_fin_seance.setMinutes(seanceMobileDTO.getHeure_fin_seance_minute());
		
		Seance seance = new Seance();
		seance.setDate_seance(seanceMobileDTO.getDate_seance());
		seance.setHeure_debut_seance(Heure_debut_seance);
		seance.setHeure_fin_seance(Heure_fin_seance);
		seance.setLecon(seanceMobileDTO.getLecon());
		seance.setRepetition(seanceMobileDTO.getRepetition());
		seance.setLieu(seanceMobileDTO.getLieu());
		seance.setStatut("NON EFFECTUEE");
		
		em.persist(seance);
		return seance;
		
	}

}
