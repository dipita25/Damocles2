package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.UtilisateurDTO;
import cm.darttc.damocles.model.Apprenant;


@Stateless
public class NouveauPlanningService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;

	
	public List<Apprenant> mesApprenants(UtilisateurDTO utilisateurDTO){
		
		Long id_repetition = utilisateurDTO.getId_personne();
		
		TypedQuery<Apprenant> mesApprenantQuery = em.createQuery("SELECT DISTINCT a FROM Apprenant a "
				+ "WHERE a.id IN (SELECT DISTINCT ra.apprenant.id FROM Repetition_Apprenant ra WHERE ra.repetition.id IN "
				+ "(SELECT DISTINCT rr.repetition.id FROM Repetition_Repetiteur rr WHERE rr.repetiteur.id = :entityIds))",Apprenant.class);
		
		mesApprenantQuery.setParameter("entityIds", id_repetition);
		
		List<Apprenant> apprenants = mesApprenantQuery.getResultList();
		
		
		return apprenants;
		
	}
	
	
}
