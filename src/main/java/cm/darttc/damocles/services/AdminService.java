package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Admin;

@Stateless
public class AdminService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Admin create(Admin admin) {
		em.persist(admin);
		return admin;
	}
	
	public Admin findById(Long id)
	{
		Admin entity = em.find(Admin.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Admin entity = em.find(Admin.class,id);
		em.remove(entity);
		
	}
	
	public Admin update(Admin admin)
	{
		em.merge(admin);
		return admin;
	}
	
	public List<Admin> findAdmins (){
		TypedQuery<Admin> AdminQuery = em.createQuery("SELECT DISTINCT a FROM Admin a ",Admin.class);
		List<Admin> resultList = AdminQuery.getResultList();
		return resultList;
	}
}
