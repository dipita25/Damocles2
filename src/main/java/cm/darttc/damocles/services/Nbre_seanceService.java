package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Seance;

@Stateless
public class Nbre_seanceService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;

	
	public int[] recherche(ApprenantDTO apprenantDTO){
		
		TypedQuery<Seance> findQuery = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s WHERE s.repetition.id IN (SELECT DISTINCT ra.repetition.id FROM Repetition_Apprenant ra "
						+ "WHERE ra.apprenant.id = :entityId)",
						Seance.class);
		findQuery.setParameter("entityId", apprenantDTO.getId());
		
		List<Seance> seances = findQuery.getResultList();
		
		int nbre_seance = seances.size();
		int nbre_seance_effectuee = 0;
		for(Seance seance : seances){
			if(seance.getStatut() == "EFFECTUEE"){
				nbre_seance_effectuee = nbre_seance_effectuee +1;
			}
		}
		int nbre_seance_restante = nbre_seance - nbre_seance_effectuee;
		int tableau[] = {nbre_seance,nbre_seance_effectuee,nbre_seance_restante};
		
		return tableau;
	}
	
}
