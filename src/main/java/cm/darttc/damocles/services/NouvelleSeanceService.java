package cm.darttc.damocles.services;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.SeanceDTO;
import cm.darttc.damocles.model.Repetition;
import cm.darttc.damocles.model.Seance;

@Stateless
public class NouvelleSeanceService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Seance enregistrer(SeanceDTO seanceDTO){
		
		TypedQuery<Repetition> findQuery = em
				.createQuery(
						"SELECT DISTINCT r FROM Repetition r WHERE r.id IN (SELECT  ra.repetition.id FROM Repetition_Apprenant ra "
						+ "WHERE ra.apprenant.id = :entityId)",
						Repetition.class);
		findQuery.setParameter("entityId", seanceDTO.getId_apprenant());
		
		Repetition repetition = findQuery.getSingleResult();
		
		seanceDTO.setRepetition(repetition);
		
		Date Heure_debut_seance = new Date();
		Heure_debut_seance.setHours(seanceDTO.getHeure_debut_seance().getHours());
		Heure_debut_seance.setMinutes(seanceDTO.getHeure_debut_seance().getMinutes());
		
		Date Heure_fin_seance = new Date();
		Heure_fin_seance.setHours(seanceDTO.getHeure_fin_seance().getHours());
		Heure_fin_seance.setMinutes(seanceDTO.getHeure_fin_seance().getMinutes());
		
		Seance seance = new Seance();
		seance.setDate_seance(seanceDTO.getDate_seance());
		seance.setHeure_debut_seance(Heure_debut_seance);
		seance.setHeure_fin_seance(Heure_fin_seance);
		seance.setLecon(seanceDTO.getLecon());
		seance.setRepetition(seanceDTO.getRepetition());
		seance.setLieu(seanceDTO.getLieu());
		seance.setStatut("NON EFFECTUEE");
		
		em.persist(seance);
		return seance;
		
	}

}
