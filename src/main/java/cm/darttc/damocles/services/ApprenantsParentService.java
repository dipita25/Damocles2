package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.UtilisateurDTO;
import cm.darttc.damocles.model.Apprenant;

@Stateless
public class ApprenantsParentService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public List<Apprenant> recherche(UtilisateurDTO utilisateurDTO){
		
		TypedQuery<Apprenant> findQuery = em
				.createQuery(
						"SELECT DISTINCT a FROM Apprenant a WHERE a.parent.id = :entityId ",
						Apprenant.class);
		findQuery.setParameter("entityId", utilisateurDTO.getId_personne());
		
		List<Apprenant> apprenants = findQuery.getResultList();
		
		
		return apprenants;
	}

}
