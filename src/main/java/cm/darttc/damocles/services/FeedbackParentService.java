package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.UtilisateurDTO;
import cm.darttc.damocles.model.Seance;

@Stateless
public class FeedbackParentService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public List<Seance> recherche(UtilisateurDTO utilisateurtDTO){
		
		TypedQuery<Seance> findQuery = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s WHERE s.statut LIKE 'NON EFFECTUEE' AND s.repetition.id IN (SELECT DISTINCT ra.repetition.id FROM Repetition_Apprenant ra "
						+ "WHERE ra.apprenant.id IN (SELECT a.id FROM Apprenant a WHERE a.parent.id = :entityId))",
						Seance.class);
		findQuery.setParameter("entityId", utilisateurtDTO.getId_personne());
		
		List<Seance> seances = findQuery.getResultList();
		
		
		return seances;
	}

}
