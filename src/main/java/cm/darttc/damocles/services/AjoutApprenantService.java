package cm.darttc.damocles.services;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.enumeration.Etat;
import cm.darttc.damocles.model.Apprenant;
import cm.darttc.damocles.model.Parent;
import cm.darttc.damocles.model.Repetition;
import cm.darttc.damocles.model.Repetition_Apprenant;

@Stateless
public class AjoutApprenantService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Repetition_Apprenant enregistrer(ApprenantDTO apprenantDTO){
		
		TypedQuery<Parent> findQuery = em
				.createQuery(
						"SELECT DISTINCT p FROM Parent p WHERE p.id = :entityId)",
						Parent.class);
		findQuery.setParameter("entityId", apprenantDTO.getId_parent());
		
		Parent parent = findQuery.getSingleResult();
		
		java.util.Date date_souscription = new java.util.Date();
		
		
		Repetition repetition = new Repetition();
		repetition.setDate_souscription(date_souscription);
		repetition.setParent(parent);
		repetition.setStatut(Etat.attente);
		em.persist(repetition);
		
		Apprenant apprenant = new Apprenant();
		apprenant.setAge(apprenantDTO.getAge());
		apprenant.setClasse(apprenantDTO.getClasse());
		apprenant.setEtablissement(apprenantDTO.getEtablissement());
		apprenant.setNom(apprenantDTO.getNom());
		apprenant.setParent(parent);
		apprenant.setPrenom(apprenantDTO.getPrenom());
		
		em.persist(apprenant);
		
		Repetition_Apprenant repetitionApprenant = new Repetition_Apprenant();
		repetitionApprenant.setApprenant(apprenant);
		repetitionApprenant.setRepetition(repetition);
		em.persist(repetitionApprenant);
		
		return repetitionApprenant;
	}
}
