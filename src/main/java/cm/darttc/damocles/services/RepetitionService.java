package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Repetition;



@Stateless
public class RepetitionService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Repetition create(Repetition repetition) {
		em.persist(repetition);
		return repetition;
	}
	
	public Repetition findById(Long id)
	{
		Repetition entity = em.find(Repetition.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Repetition entity = em.find(Repetition.class,id);
		em.remove(entity);
		
	}
	
	public Repetition update(Repetition repetition)
	{
		em.merge(repetition);
		return repetition;
	}
	
	public List<Repetition> findRepetitions (){
		TypedQuery<Repetition> ParentQuery = em.createQuery("SELECT DISTINCT r FROM Repetition r ",Repetition.class);
		List<Repetition> resultList = ParentQuery.getResultList();
		return resultList;
	}

}
