package cm.darttc.damocles.services;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import cm.darttc.damocles.DTO.MatiereApprenantDTO;
import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.model.Matiere_Repetition_Apprenant;
import cm.darttc.damocles.model.Repetition_Apprenant;

@Stateless
public class AjoutMatiereApprenantService {

	@EJB
	private Matiere_Repetition_ApprenantService matiere_Repetition_ApprenantService;
	
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Response ajoutMatiere(MatiereApprenantDTO matiereApprenantDTO){
		
		TypedQuery<Repetition_Apprenant> findQuery = em
				.createQuery(
						"SELECT DISTINCT p FROM Repetition_Apprenant p WHERE p.id = :entityId)",
						Repetition_Apprenant.class);
		findQuery.setParameter("entityId", matiereApprenantDTO.getId());
		
		Repetition_Apprenant repetition_Apprenant = findQuery.getSingleResult();
		
		System.out.println(repetition_Apprenant);
		try{
			Matiere[] matieres = matiereApprenantDTO.getMatieres();
			for (int t=0; t< matieres.length; t++){
				Matiere_Repetition_Apprenant matiere_Repetition_Apprenant = new Matiere_Repetition_Apprenant();
				matiere_Repetition_Apprenant.setMatiere(matieres[t]);
				matiere_Repetition_Apprenant.setRepetition_apprenant(repetition_Apprenant);
				matiere_Repetition_ApprenantService.create(matiere_Repetition_Apprenant);
			}
				return Response.ok().build();
			
			
		}catch (Exception e) {
			// TODO: handle exception
			return Response.status(Status.FORBIDDEN).build();
		}

	}
}
