package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.model.Matiere_Repetition_Apprenant;

@Stateless
public class Matiere_Repetition_ApprenantService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Matiere_Repetition_Apprenant create(Matiere_Repetition_Apprenant matiere_Repetition_Apprenant) {
		em.persist(matiere_Repetition_Apprenant);
		return matiere_Repetition_Apprenant;
	}
	
	public Matiere_Repetition_Apprenant findById(Long id)
	{
		Matiere_Repetition_Apprenant entity = em.find(Matiere_Repetition_Apprenant.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Matiere_Repetition_Apprenant entity = em.find(Matiere_Repetition_Apprenant.class,id);
		em.remove(entity);
		
	}
	
	public Matiere_Repetition_Apprenant update(Matiere_Repetition_Apprenant matiere_Repetition_Apprenant)
	{
		em.merge(matiere_Repetition_Apprenant);
		return matiere_Repetition_Apprenant;
	}
	
	public List<Matiere_Repetition_Apprenant> findMatiere_Repetition_Apprenant (){
		TypedQuery<Matiere_Repetition_Apprenant> Matiere_Repetition_ApprenantQuery = em.createQuery("SELECT DISTINCT m FROM Matiere_Repetition_Apprenant m ",Matiere_Repetition_Apprenant.class);
		List<Matiere_Repetition_Apprenant> resultList = Matiere_Repetition_ApprenantQuery.getResultList();
		return resultList;
	}
	
	public List<Matiere> matieres_repetition(Long id){
		
		TypedQuery<Matiere> findQuery = em
				.createQuery(
						"SELECT DISTINCT m FROM Matiere m WHERE m.id IN (SELECT DISTINCT mra.matiere.id FROM Matiere_Repetition_Apprenant mra "
						+ "WHERE mra.repetition_apprenant.id IN (SELECT DISTINCT ra.id FROM Repetition_Apprenant ra WHERE ra.repetition.id = :entityId))",
						Matiere.class);
		findQuery.setParameter("entityId", id);
		
		List<Matiere> resultList = findQuery.getResultList();
		
		
		return resultList;
		
	}

}
