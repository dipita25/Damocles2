package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Repetition_Apprenant;


@Stateless
public class RepetitionApprenantService {
	
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Repetition_Apprenant create(Repetition_Apprenant repetition_Apprenant) {
		em.persist(repetition_Apprenant);
		return repetition_Apprenant;
	}
	
	public Repetition_Apprenant findById(Long id)
	{
		Repetition_Apprenant entity = em.find(Repetition_Apprenant.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Repetition_Apprenant entity = em.find(Repetition_Apprenant.class,id);
		em.remove(entity);
		
	}
	
	public Repetition_Apprenant update(Repetition_Apprenant repetition_Apprenant)
	{
		em.merge(repetition_Apprenant);
		return repetition_Apprenant;
	}
	
	public List<Repetition_Apprenant> findRepetition_Apprenants (){
		TypedQuery<Repetition_Apprenant> Repetition_ApprenantQuery = em.createQuery("SELECT DISTINCT p FROM Parent p ",Repetition_Apprenant.class);
		List<Repetition_Apprenant> resultList = Repetition_ApprenantQuery.getResultList();
		return resultList;
	}

}
