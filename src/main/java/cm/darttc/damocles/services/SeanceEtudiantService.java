package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.ApprenantDTO;
import cm.darttc.damocles.model.Seance;

@Stateless
public class SeanceEtudiantService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public List<Seance> recherche(ApprenantDTO apprenantDTO){
		
		TypedQuery<Seance> findQuery = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s WHERE s.repetition.id IN (SELECT DISTINCT ra.repetition.id FROM Repetition_Apprenant ra "
						+ "WHERE ra.apprenant.id = :entityId)",
						Seance.class);
		findQuery.setParameter("entityId", apprenantDTO.getId());
		
		List<Seance> seances = findQuery.getResultList();
		
		
		return seances;
	}

}
