package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Matiere_Repetition;

@Stateless
public class Matiere_RepetitionService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Matiere_Repetition create(Matiere_Repetition matiere_Repetition) {
		em.persist(matiere_Repetition);
		return matiere_Repetition;
	}
	
	public Matiere_Repetition findById(Long id)
	{
		Matiere_Repetition entity = em.find(Matiere_Repetition.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Matiere_Repetition entity = em.find(Matiere_Repetition.class,id);
		em.remove(entity);
		
	}
	
	public Matiere_Repetition update(Matiere_Repetition matiere_Repetition)
	{
		em.merge(matiere_Repetition);
		return matiere_Repetition;
	}
	
	public List<Matiere_Repetition> findMatiere_Repetitions (){
		TypedQuery<Matiere_Repetition> Matiere_RepetitionQuery = em.createQuery("SELECT DISTINCT p FROM Matiere_Repetition p ",Matiere_Repetition.class);
		List<Matiere_Repetition> resultList = Matiere_RepetitionQuery.getResultList();
		return resultList;
	}

}
