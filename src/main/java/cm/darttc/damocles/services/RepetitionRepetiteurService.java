package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Repetition_Repetiteur;

@Stateless
public class RepetitionRepetiteurService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Repetition_Repetiteur create(Repetition_Repetiteur repetition_Repetiteur) {
		em.persist(repetition_Repetiteur);
		return repetition_Repetiteur;
	}
	
	public Repetition_Repetiteur findById(Long id)
	{
		Repetition_Repetiteur entity = em.find(Repetition_Repetiteur.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Repetition_Repetiteur entity = em.find(Repetition_Repetiteur.class,id);
		em.remove(entity);
		
	}
	
	public Repetition_Repetiteur update(Repetition_Repetiteur repetition_Repetiteur)
	{
		em.merge(repetition_Repetiteur);
		return repetition_Repetiteur;
	}
	
	public List<Repetition_Repetiteur> findRepetition_Repetiteurs (){
		TypedQuery<Repetition_Repetiteur> Repetition_RepetiteurQuery = em.createQuery("SELECT DISTINCT p FROM Repetition_Repetiteur p ",Repetition_Repetiteur.class);
		List<Repetition_Repetiteur> resultList = Repetition_RepetiteurQuery.getResultList();
		return resultList;
	}

}
