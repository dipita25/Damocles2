package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Matiere_Repetiteur;

@Stateless
public class MatiereRepetiteurService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Matiere_Repetiteur create(Matiere_Repetiteur matiere_Repetiteur) {
		em.persist(matiere_Repetiteur);
		return matiere_Repetiteur;
	}
	
	public Matiere_Repetiteur findById(Long id)
	{
		Matiere_Repetiteur entity = em.find(Matiere_Repetiteur.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Matiere_Repetiteur entity = em.find(Matiere_Repetiteur.class,id);
		em.remove(entity);
		
	}
	
	public Matiere_Repetiteur update(Matiere_Repetiteur matiere_Repetiteur)
	{
		em.merge(matiere_Repetiteur);
		return matiere_Repetiteur;
	}
	
	public List<Matiere_Repetiteur> findRepetitions (){
		TypedQuery<Matiere_Repetiteur> Matiere_RepetiteurQuery = em.createQuery("SELECT DISTINCT p FROM Matiere_Repetiteur p ",Matiere_Repetiteur.class);
		List<Matiere_Repetiteur> resultList = Matiere_RepetiteurQuery.getResultList();
		return resultList;
	}

}
