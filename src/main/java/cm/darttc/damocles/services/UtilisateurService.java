package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Utilisateur;



@Stateless
public class UtilisateurService {
	
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Utilisateur create(Utilisateur utilisateur) {
		em.persist(utilisateur);
		return utilisateur;
	}
	
	public Utilisateur findById(Long id)
	{
		Utilisateur entity = em.find(Utilisateur.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Utilisateur entity = em.find(Utilisateur.class,id);
		em.remove(entity);
		
	}
	
	public Utilisateur update(Utilisateur utilisateur)
	{
		em.merge(utilisateur);
		return utilisateur;
	}
	
	public List<Utilisateur> findUtilisateurs (){
		TypedQuery<Utilisateur> UtilisateurQuery = em.createQuery("SELECT DISTINCT p FROM Utilisateur p ",Utilisateur.class);
		List<Utilisateur> resultList = UtilisateurQuery.getResultList();
		return resultList;
	}

}

