package cm.darttc.damocles.services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Matiere;
import cm.darttc.damocles.model.Repetiteur;
import cm.darttc.damocles.model.Repetition;
import cm.darttc.damocles.model.Repetition_Repetiteur;

@Stateless
public class RepetiteurService {

	@EJB
	private RepetitionRepetiteurService repetitionRepetiteurService;
	
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Repetiteur create(Repetiteur repetiteur) {
		em.persist(repetiteur);
		return repetiteur;
	}
	
	public Repetiteur findById(Long id)
	{
		Repetiteur entity = em.find(Repetiteur.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Repetiteur entity = em.find(Repetiteur.class,id);
		em.remove(entity);
		
	}
	
	public Repetiteur update(Repetiteur repetiteur)
	{
		em.merge(repetiteur);
		return repetiteur;
	}
	
	public List<Repetiteur> findRepetiteurs (){
		TypedQuery<Repetiteur> RepetiteurQuery = em.createQuery("SELECT DISTINCT p FROM Repetiteur p ",Repetiteur.class);
		List<Repetiteur> resultList = RepetiteurQuery.getResultList();
		return resultList;
	}
	
	/*
	 * retourne tous les repetiteurs specialises dans au moins une des matieres de la repetition 
	 * et qui possedent le niveau scolaire demandé par le parent
	 */
	public List<Repetiteur> repetiteurs(List<Matiere> liste,Repetition repetition){
		
		//liste des repetiteurs qui sera retournée à l'utilisateur
		List<Repetiteur> resultList =  new ArrayList<Repetiteur>();
		
		for(int i=0 ; i < liste.size() ; i++){
			
			TypedQuery<Repetiteur> findQuery = em
				.createQuery(
						"SELECT DISTINCT r FROM Repetiteur r WHERE r.id IN (SELECT DISTINCT rm.repetiteur.id FROM Matiere_Repetiteur rm "
						+ "WHERE rm.matiere.id = :entityId ) AND r.niveau_scolaire = :entityrepetition",
						Repetiteur.class);
			findQuery.setParameter("entityId", liste.get(i).getId());
			findQuery.setParameter("entityrepetition", repetition.getNiveau_scolaire_repetiteur());
			
			//la liste contenant tout repetiteur specialisé dans au moins une des matieres de la repetition souhaitée
			List<Repetiteur> repetiteurs = findQuery.getResultList();
			System.out.println("*********************************************liste  "+liste.get(i));
			System.out.println("*********************************************repetiteur taille "+repetiteurs.size());
			
			//si la liste à retourner est encore vide
			if(resultList.size() == 0){
				for(int k=0 ; k < repetiteurs.size(); k++){
					resultList.add(repetiteurs.get(k));
					System.out.println("********************************************************************************************1resultList ajout");
					System.out.println("********************************************************************************************1resultList.size()"+ resultList.size());
				}
			}
			else{
				//verifions que ce repetiteur n'existe pas dejà dans la liste à retourner à l'utilisateur 
				
				for(Repetiteur o : repetiteurs){
					if(resultList.contains(o)){
						
					}
					else {
						resultList.add(o);
					}
					
				}
			}
		}
		
		return resultList;
		
	}
	
	//permet d'obtenir tous les repetiteurs d'une repetition dont l'id est connue
	public List<Repetiteur> Lesrepetiteurs(Long id){
		
		
			
			TypedQuery<Repetiteur> findQuery = em
				.createQuery(
						"SELECT DISTINCT r FROM Repetiteur r WHERE r.id IN (SELECT DISTINCT rm.repetiteur.id FROM Repetition_Repetiteur rm "
						+ "WHERE rm.repetition.id = :entityId )",
						Repetiteur.class);
			findQuery.setParameter("entityId", id);
			
			List<Repetiteur> resultList = findQuery.getResultList();
			
			//verifions que ce repetiteur n'existe pas dejà dans la liste des repetiteurs 
			
		
		return resultList;
		
	}
	
	
	/*
	 * permet d'attribuer des repetiteurs à une repetition
	 */
	public List<Repetiteur> matieres_repetiteur(List<Repetiteur> liste_repetiteur_specialise , List<Matiere> liste_matieres_repetition, Repetition repetition){
		
		List<Matiere> resultList = liste_matieres_repetition;
		
		//liste des repetiteurs validés pour la repetition à retourner
		List<Repetiteur> repetiteurs_validés = new ArrayList<Repetiteur>();
		
		//liste des repetiteurs specialises dans les matieres de la repetition mais qui ont déjà au moins une repetition en charge
		List<Repetiteur> repetiteurs_occupes = new ArrayList<Repetiteur>();
		
		if(liste_repetiteur_specialise.size() != 0){
		//on parcours la liste des repetiteurs specialises dans les matieres de la repetition
		for(Repetiteur repetiteur_specialise : liste_repetiteur_specialise){
			TypedQuery<Matiere> findQuery = em
				.createQuery(
						"SELECT DISTINCT m FROM Matiere m WHERE m.id IN (SELECT DISTINCT mr.matiere.id FROM Matiere_Repetiteur mr "
						+ "WHERE mr.repetiteur.id = :entityId )",
						Matiere.class);
			findQuery.setParameter("entityId", repetiteur_specialise.getId());
			
			//on recupere les matieres de specialités du repetiteur courant
			List<Matiere> matieres_repetiteur = findQuery.getResultList();
			
			//on parcours les matières de ce repetiteur specialisé
			for(Matiere o : matieres_repetiteur){
				//si la matiere courante est trouvée dans la liste des matieres de la repetition
				if(resultList.contains(o)){
					
					TypedQuery<Repetiteur> findQuery4 = em
							.createQuery(
									"SELECT DISTINCT r FROM Repetiteur r WHERE r.id IN (SELECT DISTINCT rr.repetiteur.id FROM Repetition_Repetiteur rr)",
											Repetiteur.class);
					
					//liste des repetiteurs de toutes les repetitions
					List<Repetiteur> repetiteurs_toutes_repetition = findQuery4.getResultList();
					
					//si ce repetiteur specialise appartient dejà à une repetition
					if(repetiteurs_toutes_repetition.contains(repetiteur_specialise)){
						
						//on l'ajoute dans la liste des repetiteurs specialiés mais ayant deja au moins une repetition en cours
						repetiteurs_occupes.add(repetiteur_specialise);
					}
					//si ce repetiteur specialise est libre
					else{
						//on enleve cette matiere du tableaau des matieres de la repetition
						resultList.remove(o);
						
						//on recupere une nvlle liste de repetiteur specialises dans les matieres restantes
						liste_repetiteur_specialise = repetiteurs(resultList,repetition);
						
						//on affecte ce repetiteur specialisé à la repetition
						Repetition_Repetiteur repetition_repetiteur = new Repetition_Repetiteur();
						repetition_repetiteur.setRepetiteur(repetiteur_specialise);
						repetition_repetiteur.setRepetition(repetition);
						repetitionRepetiteurService.create(repetition_repetiteur);
						
						repetiteurs_validés.add(repetiteur_specialise);
					}
				}
			}
		}
		}
		
		//liste des repetiteurs de la repetition
		List<Repetiteur> repetiteurs_repetition = Lesrepetiteurs(repetition.getId());
		
		//ceci va s'executer si tous les repetiteurs specialisés dans les matieres de la repetition sont déjà occupés dans d'autres repetitions
		if(repetiteurs_repetition.isEmpty()){
			for(Repetiteur r: repetiteurs_occupes){
				
				TypedQuery<Matiere> findQuery10 = em
						.createQuery(
								"SELECT DISTINCT m FROM Matiere m WHERE m.id IN (SELECT DISTINCT mr.matiere.id FROM Matiere_Repetiteur mr "
								+ "WHERE mr.repetiteur.id = :entityId )",
								Matiere.class);
					findQuery10.setParameter("entityId",r.getId());
					
					//on recupere les matieres de specialités du repetiteur courant
					List<Matiere> matieres_repetiteur = findQuery10.getResultList();
					
					//on parcours les matieres du repetiteur courant
					for(Matiere o : matieres_repetiteur){
						
						//si la matiere courante se trouve dans la liste des matieres de la repetition
						if(resultList.contains(o)){
							
							//on enleve cette matiere du tableaau des matieres de la repetition
							resultList.remove(o);
							
							//on recupere une nvlle liste de repetiteur specialises deja occupés dans les matieres restantes
							repetiteurs_occupes = repetiteurs(resultList,repetition);
							
							//on affecte ce repetiteur specialisé à la repetition
							Repetition_Repetiteur repetition_repetiteur = new Repetition_Repetiteur();
							repetition_repetiteur.setRepetiteur(r);
							repetition_repetiteur.setRepetition(repetition);
							repetitionRepetiteurService.create(repetition_repetiteur);
							
							//on ajoute ce repetiteur à la liste qu'on va retourner
							repetiteurs_validés.add(r);
							
						}
					}
			}
		}
		
		return repetiteurs_validés;
	}

}
