package cm.darttc.damocles.services;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.FeedbackMobileDTO;
import cm.darttc.damocles.model.Feedback;
import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.model.Utilisateur;

@Stateless
public class EnregistrerFeedbackMobileService {
	

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Feedback enregistrer(FeedbackMobileDTO feedbackMobileDTO){
		
		TypedQuery<Seance> findQuery1 = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s WHERE s.id = :entityId)",
						Seance.class);
		findQuery1.setParameter("entityId", feedbackMobileDTO.getId_seance());
		
		Seance seance = findQuery1.getSingleResult();
		
		
		TypedQuery<Utilisateur> findQuery2 = em
				.createQuery(
						"SELECT DISTINCT u FROM Utilisateur u WHERE u.id_personne = :entityId)",
						Utilisateur.class);
		findQuery2.setParameter("entityId", feedbackMobileDTO.getId_auteur());
		
		Utilisateur utilisateur = findQuery2.getSingleResult();
		
		
		Feedback feedback = new Feedback();
		
		
		
		Date heure_arrivee = new Date();
		heure_arrivee.setHours(feedbackMobileDTO.getHeure_arrivee());
		heure_arrivee.setMinutes(feedbackMobileDTO.getHeure_arrivee_minute());
		
		Date heure_debut = new Date();
		heure_debut.setHours(feedbackMobileDTO.getHeure_debut());
		heure_debut.setMinutes(feedbackMobileDTO.getHeure_debut_minute());
		
		Date heure_depart = new Date();
		heure_depart.setHours(feedbackMobileDTO.getHeure_depart());
		heure_depart.setMinutes(feedbackMobileDTO.getHeure_depart_minute());
		
		Date heure_fin = new Date();
		heure_fin.setHours(feedbackMobileDTO.getHeure_fin());
		heure_fin.setMinutes(feedbackMobileDTO.getHeure_fin_minute());
		
		
		feedback.setAppreciation(feedbackMobileDTO.getAppreciation());
		feedback.setAuteur(utilisateur);
		feedback.setHeure_arrivee(heure_arrivee);
		feedback.setHeure_debut(heure_debut);
		feedback.setHeure_depart(heure_depart);
		feedback.setHeure_fin(heure_fin);
		feedback.setSeance(seance);
		seance.setStatut("EFFECTUEE");
		em.persist(feedback);
		
		return feedback;
	}

}
