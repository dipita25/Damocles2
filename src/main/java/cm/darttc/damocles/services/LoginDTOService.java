package cm.darttc.damocles.services;



import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.LoginDTO;
import cm.darttc.damocles.model.Admin;
import cm.darttc.damocles.model.Parent;
import cm.darttc.damocles.model.Repetiteur;
import cm.darttc.damocles.model.Utilisateur;


@Stateless
public class LoginDTOService {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public 	Utilisateur authenticate(LoginDTO loginDTO)
	{
		
		TypedQuery<Admin> findQueryAdmin = em
				.createQuery(
						"SELECT  a FROM Admin a "
						+ "WHERE a.login = :entityLogin AND a.password = :entityPassword",
						Admin.class);
		findQueryAdmin.setParameter("entityLogin", loginDTO.getLogin());
		findQueryAdmin.setParameter("entityPassword", loginDTO.getPassword());
		
		
		TypedQuery<Parent> findQueryParent = em
				.createQuery(
						"SELECT  p FROM Parent p "
						+ "WHERE p.login = :entityLogin AND p.password = :entityPassword",
						Parent.class);
		findQueryParent.setParameter("entityLogin", loginDTO.getLogin());
		findQueryParent.setParameter("entityPassword", loginDTO.getPassword());
		
		TypedQuery<Repetiteur> findQueryRepetiteur = em
				.createQuery(
						"SELECT  r FROM Repetiteur r "
						+ "WHERE r.login = :entityLogin AND r.password = :entityPassword",
						Repetiteur.class);
		findQueryRepetiteur.setParameter("entityLogin", loginDTO.getLogin());
		findQueryRepetiteur.setParameter("entityPassword", loginDTO.getPassword());
		
		
		Admin admin = new Admin();
		Parent parent = new Parent();
		Repetiteur repetiteur = new Repetiteur();
		Utilisateur utilisateur = new Utilisateur();
		
		/*try{*/
			try{
				admin = findQueryAdmin.getSingleResult();
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
					findQueryUtilisateur.setParameter("entityId",admin.getId() );
					findQueryUtilisateur.setParameter("entityType","admin" );
					
					utilisateur = findQueryUtilisateur.getSingleResult();
					
					if(utilisateur.getType_utilisateur() != "null"){

						return utilisateur;
					}
			}
			catch(Exception e){
				
			}
			
			try{
				parent = findQueryParent.getSingleResult();
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",parent.getId() );
				findQueryUtilisateur.setParameter("entityType","parent" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				
				if(utilisateur.getType_utilisateur() != "null"){

					return utilisateur;
				}
				
			}
			catch(Exception e){
				
			}
			try{
				repetiteur = findQueryRepetiteur.getSingleResult();
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",repetiteur.getId() );
				findQueryUtilisateur.setParameter("entityType","repetiteur" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				
				if(utilisateur.getType_utilisateur() != "null"){

					return utilisateur;
				}
			}
			catch(Exception e){
				
			}
			/*if(admin != null){
			
				TypedQuery<Utilisateur> findQueryUtilisateur = em
					.createQuery(
							"SELECT  u FROM Utilisateur u "
							+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
							Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",admin.getId() );
				findQueryUtilisateur.setParameter("entityType","admin" );
				
				utilisateur = findQueryUtilisateur.getSingleResult();
				return utilisateur;
			}*/
			/*else if(parent != null){
				
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",parent.getId() );
				findQueryUtilisateur.setParameter("entityType","parent" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				return utilisateur;
					
			}*/
			/*else if(repetiteur != null){
				
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",repetiteur.getId() );
				findQueryUtilisateur.setParameter("entityType","repetiteur" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				return utilisateur;
			}
			else{*/
			
				/*utilisateur = new Utilisateur();*/
				return utilisateur;
			/*}*/
			
			
			
		/*}
		catch(Exception e){
			utilisateur = new Utilisateur();
			return  utilisateur;
			
		}*/
	}

}
