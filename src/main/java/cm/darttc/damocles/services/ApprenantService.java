package cm.darttc.damocles.services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.model.Apprenant;

@Stateless
public class ApprenantService {
	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Apprenant create(Apprenant apprenant) {
		em.persist(apprenant);
		return apprenant;
	}
	
	public Apprenant findById(Long id)
	{
		Apprenant entity = em.find(Apprenant.class,id);
		return entity;
	}
	
	public void deleteById(Long id)
	{
		Apprenant entity = em.find(Apprenant.class,id);
		em.remove(entity);
		
	}
	
	public Apprenant update(Apprenant apprenant)
	{
		em.merge(apprenant);
		return apprenant;
	}
	
	public List<Apprenant> findAdmins (){
		TypedQuery<Apprenant> ApprenantQuery = em.createQuery("SELECT DISTINCT a FROM Apprenant a ",Apprenant.class);
		List<Apprenant> resultList = ApprenantQuery.getResultList();
		return resultList;
	}
	
}