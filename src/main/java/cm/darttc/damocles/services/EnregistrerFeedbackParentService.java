package cm.darttc.damocles.services;

import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.FeedbackDTO;
import cm.darttc.damocles.model.Feedback;
import cm.darttc.damocles.model.Seance;
import cm.darttc.damocles.model.Utilisateur;

@Stateless
public class EnregistrerFeedbackParentService {
	


	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public Feedback enregistrer(FeedbackDTO feedbackDTO){
		
		TypedQuery<Seance> findQuery1 = em
				.createQuery(
						"SELECT DISTINCT s FROM Seance s WHERE s.id = :entityId)",
						Seance.class);
		findQuery1.setParameter("entityId", feedbackDTO.getId_seance());
		
		Seance seance = findQuery1.getSingleResult();
		
		
		TypedQuery<Utilisateur> findQuery2 = em
				.createQuery(
						"SELECT DISTINCT u FROM Utilisateur u WHERE u.id_personne = :entityId)",
						Utilisateur.class);
		findQuery2.setParameter("entityId", feedbackDTO.getId_auteur());
		
		Utilisateur utilisateur = findQuery2.getSingleResult();
		
		
		Feedback feedback = new Feedback();
		
		
		
		Date heure_arrivee = new Date();
		heure_arrivee.setHours(feedbackDTO.getHeure_arrivee().getHours());
		heure_arrivee.setMinutes(feedbackDTO.getHeure_arrivee().getMinutes());
		
		Date heure_debut = new Date();
		heure_debut.setHours(feedbackDTO.getHeure_debut().getHours());
		heure_debut.setMinutes(feedbackDTO.getHeure_debut().getMinutes());
		
		Date heure_depart = new Date();
		heure_depart.setHours(feedbackDTO.getHeure_depart().getHours());
		heure_depart.setMinutes(feedbackDTO.getHeure_depart().getMinutes());
		
		Date heure_fin = new Date();
		heure_fin.setHours(feedbackDTO.getHeure_fin().getHours());
		heure_fin.setMinutes(feedbackDTO.getHeure_fin().getMinutes());
		
		
		feedback.setAppreciation(feedbackDTO.getAppreciation());
		feedback.setAuteur(utilisateur);
		feedback.setHeure_arrivee(heure_arrivee);
		feedback.setHeure_debut(heure_debut);
		feedback.setHeure_depart(heure_depart);
		feedback.setHeure_fin(heure_fin);
		feedback.setSeance(seance);
		seance.setStatut("EFFECTUEE");
		em.persist(feedback);
		
		return feedback;
	}

}
