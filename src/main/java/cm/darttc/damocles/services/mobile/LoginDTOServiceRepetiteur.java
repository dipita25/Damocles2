package cm.darttc.damocles.services.mobile;



import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import cm.darttc.damocles.DTO.LoginDTO;
import cm.darttc.damocles.model.Repetiteur;
import cm.darttc.damocles.model.Utilisateur;


@Stateless
public class LoginDTOServiceRepetiteur {

	@PersistenceContext(unitName = "Damocles-persistence-unit")
	private EntityManager em;
	
	public 	Utilisateur authenticate(LoginDTO loginDTO)
	{
		
		TypedQuery<Repetiteur> findQueryRepetiteur = em
				.createQuery(
						"SELECT  r FROM Repetiteur r "
						+ "WHERE r.login = :entityLogin AND r.password = :entityPassword",
						Repetiteur.class);
		findQueryRepetiteur.setParameter("entityLogin", loginDTO.getLogin());
		findQueryRepetiteur.setParameter("entityPassword", loginDTO.getPassword());
		
		
		
		Repetiteur repetiteur = new Repetiteur();
		Utilisateur utilisateur = new Utilisateur();
		
		/*try{*/
			
			try{
				repetiteur = findQueryRepetiteur.getSingleResult();
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",repetiteur.getId() );
				findQueryUtilisateur.setParameter("entityType","repetiteur" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				
				if(utilisateur.getType_utilisateur() != "null"){

					return utilisateur;
				}
			}
			catch(Exception e){
				
			}
			/*if(admin != null){
			
				TypedQuery<Utilisateur> findQueryUtilisateur = em
					.createQuery(
							"SELECT  u FROM Utilisateur u "
							+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
							Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",admin.getId() );
				findQueryUtilisateur.setParameter("entityType","admin" );
				
				utilisateur = findQueryUtilisateur.getSingleResult();
				return utilisateur;
			}*/
			/*else if(parent != null){
				
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",parent.getId() );
				findQueryUtilisateur.setParameter("entityType","parent" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				return utilisateur;
					
			}*/
			/*else if(repetiteur != null){
				
				TypedQuery<Utilisateur> findQueryUtilisateur = em
						.createQuery(
								"SELECT  u FROM Utilisateur u "
								+ "WHERE u.id_personne = :entityId AND u.type_utilisateur = :entityType",
								Utilisateur.class);
				findQueryUtilisateur.setParameter("entityId",repetiteur.getId() );
				findQueryUtilisateur.setParameter("entityType","repetiteur" );
					
				utilisateur = findQueryUtilisateur.getSingleResult();
				return utilisateur;
			}
			else{*/
			
				/*utilisateur = new Utilisateur();*/
				return utilisateur;
			/*}*/
			
			
			
		/*}
		catch(Exception e){
			utilisateur = new Utilisateur();
			return  utilisateur;
			
		}*/
	}

}
