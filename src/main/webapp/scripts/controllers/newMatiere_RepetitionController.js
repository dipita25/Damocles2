
angular.module('damocles').controller('NewMatiere_RepetitionController', function ($scope,$window,$rootScope, $location, locationParser, flash, Matiere_RepetitionResource , MatiereResource, RepetitionResource) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.matiere_Repetition = $scope.matiere_Repetition || {};
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.matiereList = MatiereResource.queryAll(function(items){
        $scope.matiereSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.id
            });
        });
    });
    $scope.$watch("matiereSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.matiere_Repetition.matiere = {};
            $scope.matiere_Repetition.matiere.id = selection.value;
        }
    });
    
    $scope.repetitionList = RepetitionResource.queryAll(function(items){
        $scope.repetitionSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.id
            });
        });
    });
    $scope.$watch("repetitionSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.matiere_Repetition.repetition = {};
            $scope.matiere_Repetition.repetition.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The matiere_Repetition was created successfully.'});
            $location.path('/Matiere_Repetitions');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        Matiere_RepetitionResource.save($scope.matiere_Repetition, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Matiere_Repetitions");
    };
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});