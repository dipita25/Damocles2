

angular.module('damocles').controller('EditRepetition_ApprenantController', function($scope,$window,$rootScope, $routeParams, $location, flash, Repetition_ApprenantResource , ApprenantResource, RepetitionResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.repetition_Apprenant = new Repetition_ApprenantResource(self.original);
            ApprenantResource.queryAll(function(items) {
                $scope.apprenantSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.repetition_Apprenant.apprenant && item.id == $scope.repetition_Apprenant.apprenant.id) {
                        $scope.apprenantSelection = labelObject;
                        $scope.repetition_Apprenant.apprenant = wrappedObject;
                        self.original.apprenant = $scope.repetition_Apprenant.apprenant;
                    }
                    return labelObject;
                });
            });
            RepetitionResource.queryAll(function(items) {
                $scope.repetitionSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.repetition_Apprenant.repetition && item.id == $scope.repetition_Apprenant.repetition.id) {
                        $scope.repetitionSelection = labelObject;
                        $scope.repetition_Apprenant.repetition = wrappedObject;
                        self.original.repetition = $scope.repetition_Apprenant.repetition;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetition_Apprenant could not be found.'});
            $location.path("/Repetition_Apprenants");
        };
        Repetition_ApprenantResource.get({Repetition_ApprenantId:$routeParams.Repetition_ApprenantId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.repetition_Apprenant);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The repetition_Apprenant was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.repetition_Apprenant.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Repetition_Apprenants");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetition_Apprenant was deleted.'});
            $location.path("/Repetition_Apprenants");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.repetition_Apprenant.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("apprenantSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.repetition_Apprenant.apprenant = {};
            $scope.repetition_Apprenant.apprenant.id = selection.value;
        }
    });
    $scope.$watch("repetitionSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.repetition_Apprenant.repetition = {};
            $scope.repetition_Apprenant.repetition.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});