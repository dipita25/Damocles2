

angular.module('damocles').controller('EditApprenantController', function($scope,$window,$rootScope, $routeParams, $location, flash, ApprenantResource , ParentResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(data){
            self.original = data;
            $scope.apprenant = new ApprenantResource(self.original);
            ParentResource.queryAll(function(items) {
                $scope.parentSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.apprenant.parent && item.id == $scope.apprenant.parent.id) {
                        $scope.parentSelection = labelObject;
                        $scope.apprenant.parent = wrappedObject;
                        self.original.parent = $scope.apprenant.parent;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The apprenant could not be found.'});
            $location.path("/Apprenants");
        };
        /*ApprenantResource.get({ApprenantId:$routeParams.ApprenantId}, successCallback, errorCallback);*/
        var ApprenantId = {ApprenantId:$routeParams.ApprenantId};
        
        ApprenantResource.query(ApprenantId.ApprenantId).then(function(data){
    		console.log("success total");
    		self.original = data;
            $scope.apprenant = data;
            
            ParentResource.queryAll().then(function(data){
        		console.log("success total");
        		$scope.parentSelectionList = $.map(data, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.apprenant.parent && item.id == $scope.apprenant.parent.id) {
                        $scope.parentSelection = labelObject;
                        $scope.apprenant.parent = wrappedObject;
                        self.original.parent = $scope.apprenant.parent;
                    }
                    return labelObject;
                });
    		
    			},function(error){
    				console.log("erreur");
    		});
            //$scope.apprenant = new ApprenantResource(self.original);
    		$scope.apprenant = data;
		
			},function(error){
				console.log("erreur");
				/*var t = {ApprenantId:$routeParams.ApprenantId};
				console.log(t.ApprenantId);*/
				$location.path("/Apprenants");
		});
    };

    /*$scope.isClean = function() {
    	console.log(self.original);
    	console.log($scope.apprenant);
        return angular.equals(self.original, $scope.apprenant);
    };*/

    $scope.save = function() {
       // $scope.apprenant.$update().then(function(data){
    	$rootScope.showSpinner = true;

        ApprenantResource.update($scope.apprenant).then(function(data){
    		console.log("success total");
    		$scope.get();
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };

    $scope.cancel = function() {
        $location.path("/Apprenants");
    };

    $scope.remove = function() {
        /*var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The apprenant was deleted.'});
            $location.path("/Apprenants");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.apprenant.$remove(successCallback, errorCallback);*/
        
        ApprenantResource.supprimer($scope.apprenant.id).then(function(data){
		console.log("success total");
		$location.path("/Apprenants");
	
		},function(error){
			console.log("erreur");
			if(error && error.data && error.data.message) {
                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
		});
    };
    
    
    
    $scope.$watch("parentSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.apprenant.parent = {};
            $scope.apprenant.parent.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});