

angular.module('damocles').controller('DeconnexionController', function($scope,$window,$rootScope, $routeParams, $location, flash, AdminResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($window.localStorage.getItem("id_personne")){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
    }

	$location.path("#/");
});