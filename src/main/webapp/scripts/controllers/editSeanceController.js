

angular.module('damocles').controller('EditSeanceController', function($scope, $window,$routeParams,$rootScope, $location, flash, SeanceResource , RepetitionResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(data){
            self.original = data;
            $scope.seance = new SeanceResource(self.original);
            RepetitionResource.queryAll(function(items) {
                $scope.repetitionSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.seance.repetition && item.id == $scope.seance.repetition.id) {
                        $scope.repetitionSelection = labelObject;
                        $scope.seance.repetition = wrappedObject;
                        self.original.repetition = $scope.seance.repetition;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The seance could not be found.'});
            $location.path("/Seances");
        };
        SeanceResource.get({SeanceId:$routeParams.SeanceId}, successCallback, errorCallback);*/
    	var SeanceId = {SeanceId:$routeParams.SeanceId};
        //MatiereResource.get({MatiereId:$routeParams.MatiereId}, successCallback, errorCallback);
    	SeanceResource.query(SeanceId.SeanceId).then(function(data){
    		console.log("success total");
    		self.original = data;
            $scope.seance = data;
            
            RepetitionResource.queryAll().then(function(data){
        		console.log("success total");
        		$scope.repetitionSelectionList = $.map(data, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.seance.repetition && item.id == $scope.seance.repetition.id) {
                        $scope.repetitionSelection = labelObject;
                        $scope.seance.repetition = wrappedObject;
                        self.original.repetition = $scope.seance.repetition;
                    }
                    return labelObject;
                });
    		
    			},function(error){
    				console.log("erreur");
    		});
		
			},function(error){
				console.log("erreur");
				$location.path("/Seances");
		});
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.seance);
    };

    $scope.save = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(){
            flash.setMessage({'type':'success','text':'The seance was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.seance.$update(successCallback, errorCallback);*/
        SeanceResource.update($scope.seance).then(function(data){
    		console.log("success total");
    		$scope.get();
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };

    $scope.cancel = function() {
        $location.path("/Seances");
    };

    $scope.remove = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The seance was deleted.'});
            $location.path("/Seances");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.seance.$remove(successCallback, errorCallback);*/
        var SeanceId = {SeanceId:$routeParams.SeanceId};
        //MatiereResource.get({MatiereId:$routeParams.MatiereId}, successCallback, errorCallback);
        SeanceResource.supprimer(SeanceId.SeanceId).then(function(data){
    		console.log("success total");
    		$location.path("/Seances");
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };
    
    $scope.$watch("repetitionSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.seance.repetition = {};
            $scope.seance.repetition.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});