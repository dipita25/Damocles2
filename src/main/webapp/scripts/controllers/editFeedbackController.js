

angular.module('damocles').controller('EditFeedbackController', function($scope,$window,$rootScope, $routeParams, $location, flash, FeedbackResource , SeanceResource, UtilisateurResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(data){
            self.original = data;
            $scope.feedback = new FeedbackResource(self.original);
            SeanceResource.queryAll(function(items) {
                $scope.seanceSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.feedback.seance && item.id == $scope.feedback.seance.id) {
                        $scope.seanceSelection = labelObject;
                        $scope.feedback.seance = wrappedObject;
                        self.original.seance = $scope.feedback.seance;
                    }
                    return labelObject;
                });
            });
            UtilisateurResource.queryAll(function(items) {
                $scope.auteurSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.feedback.auteur && item.id == $scope.feedback.auteur.id) {
                        $scope.auteurSelection = labelObject;
                        $scope.feedback.auteur = wrappedObject;
                        self.original.auteur = $scope.feedback.auteur;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The feedback could not be found.'});
            $location.path("/Feedbacks");
        };
        FeedbackResource.get({FeedbackId:$routeParams.FeedbackId}, successCallback, errorCallback);*/
    	var FeedbackId = {FeedbackId:$routeParams.FeedbackId};
    	FeedbackResource.query(FeedbackId.FeedbackId).then(function(data){
    		console.log("success total");
    		self.original = data;
            $scope.feedback = data;
            SeanceResource.queryAll().then(function(data){
        		console.log("success total");
        		$scope.seanceSelectionList = $.map(data, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.feedback.seance && item.id == $scope.feedback.seance.id) {
                        $scope.seanceSelection = labelObject;
                        $scope.feedback.seance = wrappedObject;
                        self.original.seance = $scope.feedback.seance;
                    }
                    return labelObject;
                });
    		
    			},function(error){
    				console.log("erreur");
    		});
            
            UtilisateurResource.queryAll().then(function(data){
        		console.log("success total");
        		$scope.auteurSelectionList = $.map(data, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.feedback.auteur && item.id == $scope.feedback.auteur.id) {
                        $scope.auteurSelection = labelObject;
                        $scope.feedback.auteur = wrappedObject;
                        self.original.auteur = $scope.feedback.auteur;
                    }
                    return labelObject;
                });
    		
    			},function(error){
    				console.log("erreur");
    		});
		
			},function(error){
				console.log("erreur");
				$location.path("/Feedbacks");
		});
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.feedback);
    };

    $scope.save = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(){
            flash.setMessage({'type':'success','text':'The feedback was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.feedback.$update(successCallback, errorCallback);*/
        FeedbackResource.update($scope.feedback).then(function(data){
    		console.log("success total");
    		$scope.get();
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };

    $scope.cancel = function() {
        $location.path("/Feedbacks");
    };

    $scope.remove = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The feedback was deleted.'});
            $location.path("/Feedbacks");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.feedback.$remove(successCallback, errorCallback);*/
        var FeedbackId = {FeedbackId:$routeParams.FeedbackId};
    	FeedbackResource.supprimer(FeedbackId.FeedbackId).then(function(data){
    		console.log("success total");
    		$location.path("/Feedbacks");
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };
    
    $scope.$watch("seanceSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.feedback.seance = {};
            $scope.feedback.seance.id = selection.value;
        }
    });
    $scope.$watch("auteurSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.feedback.auteur = {};
            $scope.feedback.auteur.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});