

angular.module('damocles').controller('FeedbackRepetiteurController', function($scope,$window,$rootScope,
		$routeParams, $location, flash,$window, UtilisateurResource,FeedbackParentFactoryResource ,EnregistrerFeedbackParentResource,FeedbackRepetiteurFactoryResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.seancesResults = [];
    $scope.feedbackDTO = $scope.feedbackDTO || {};
    $scope.utilisateurDTO = $scope.utilisateurDTO || {};
    $scope.SeanceDTO = $scope.SeanceDTO || {};
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    /* on recupere l'id de l'auteur du feedback */
    $scope.feedbackDTO.id_auteur = $window.localStorage.getItem("id_personne");
    $scope.utilisateurDTO.id_personne = $window.localStorage.getItem("id_personne");
    $scope.utilisateurDTO.type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    var date_seance = new Date();
    
    recherche = function(){
    	$rootScope.showSpinner = true;
    	
    	FeedbackRepetiteurFactoryResource.listerSeance($scope.utilisateurDTO).then(function(data){
    		console.log("success total");
    		$scope.seancesResults = data;
    		
    	},function(error){
    		console.log("erreur");
        	
        });
    	
    	//FeedbackRepetiteurFactoryResource.listerSeance($scope.utilisateurDTO,successCallbackSeances,errorCallbackSeances);
    }
    
    $scope.cancel = function(){
    	$scope.feedbackDTO = {};
    }
    
    $scope.selection = function(seance){
    	var id_seance = seance.id;
    	console.log(id_seance);
    	
    	/* on recupere l'id de la seance selectionnee */
    	$scope.feedbackDTO.id_seance = seance.id;
    	/*$scope.feedbackDTO.heure_Depart = $scope.feedbackDTO.heure_depart;
    	$scope.feedbackDTO.heure_Arrivee = $scope.feedbackDTO.heure_arrivee;
    	$scope.feedbackDTO.heure_Debut = $scope.feedbackDTO.heure_debut;
    	$scope.feedbackDTO.heure_Fin = $scope.feedbackDTO.heure_fin;*/
    	console.log($scope.feedbackDTO.heure_fin);
    }
    
    $scope.save = function(){
    	
    	$rootScope.showSpinner = true;
    	console.log($scope.feedbackDTO);
    	
    	EnregistrerFeedbackParentResource.enregistrer($scope.feedbackDTO).then(function(data){
    		console.log("success total");
    		
    	},function(error){
    		console.log("erreur");
        	
        });
    };
    
    recherche();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});