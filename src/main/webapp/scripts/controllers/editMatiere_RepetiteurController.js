

angular.module('damocles').controller('EditMatiere_RepetiteurController', function($scope,$window,$rootScope, $routeParams, $location, flash, Matiere_RepetiteurResource , RepetiteurResource, MatiereResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.matiere_Repetiteur = new Matiere_RepetiteurResource(self.original);
            RepetiteurResource.queryAll(function(items) {
                $scope.repetiteurSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.matiere_Repetiteur.repetiteur && item.id == $scope.matiere_Repetiteur.repetiteur.id) {
                        $scope.repetiteurSelection = labelObject;
                        $scope.matiere_Repetiteur.repetiteur = wrappedObject;
                        self.original.repetiteur = $scope.matiere_Repetiteur.repetiteur;
                    }
                    return labelObject;
                });
            });
            MatiereResource.queryAll(function(items) {
                $scope.matiereSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.matiere_Repetiteur.matiere && item.id == $scope.matiere_Repetiteur.matiere.id) {
                        $scope.matiereSelection = labelObject;
                        $scope.matiere_Repetiteur.matiere = wrappedObject;
                        self.original.matiere = $scope.matiere_Repetiteur.matiere;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The matiere_Repetiteur could not be found.'});
            $location.path("/Matiere_Repetiteurs");
        };
        Matiere_RepetiteurResource.get({Matiere_RepetiteurId:$routeParams.Matiere_RepetiteurId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.matiere_Repetiteur);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The matiere_Repetiteur was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.matiere_Repetiteur.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Matiere_Repetiteurs");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The matiere_Repetiteur was deleted.'});
            $location.path("/Matiere_Repetiteurs");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.matiere_Repetiteur.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("repetiteurSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.matiere_Repetiteur.repetiteur = {};
            $scope.matiere_Repetiteur.repetiteur.id = selection.value;
        }
    });
    $scope.$watch("matiereSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.matiere_Repetiteur.matiere = {};
            $scope.matiere_Repetiteur.matiere.id = selection.value;
        }
    });
    
    $scope.get();
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});