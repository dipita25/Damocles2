
angular.module('damocles').controller('NewRepetition_ApprenantController', function ($scope,$window,$rootScope, $location, locationParser, flash, Repetition_ApprenantResource , ApprenantResource, RepetitionResource) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.repetition_Apprenant = $scope.repetition_Apprenant || {};
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.apprenantList = ApprenantResource.queryAll(function(items){
        $scope.apprenantSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.id
            });
        });
    });
    $scope.$watch("apprenantSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.repetition_Apprenant.apprenant = {};
            $scope.repetition_Apprenant.apprenant.id = selection.value;
        }
    });
    
    $scope.repetitionList = RepetitionResource.queryAll(function(items){
        $scope.repetitionSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.id
            });
        });
    });
    $scope.$watch("repetitionSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.repetition_Apprenant.repetition = {};
            $scope.repetition_Apprenant.repetition.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The repetition_Apprenant was created successfully.'});
            $location.path('/Repetition_Apprenants');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        Repetition_ApprenantResource.save($scope.repetition_Apprenant, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Repetition_Apprenants");
    };
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});