

angular.module('damocles').controller('InscriptionRepetiteurController', function($scope,$location,$window,$rootScope, $routeParams, $location, flash, RepetiteurResource ,MatiereResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.InscriptionRepetiteurDTO = $scope.InscriptionRepetiteurDTO || {};
    $scope.searchResults = [];
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		//$location.path("#/");
    }
    
    var nouveau_tableau_matiere = [];
    $rootScope.showSpinner = true;
    MatiereResource.queryAll().then(function(data){
		console.log("success total");
		$scope.searchResults = data;
	
		},function(error){
			console.log("erreur");
			if(error && error.data && error.data.message) {
                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
	});
    
    
    $scope.save = function() {
        $rootScope.showSpinner = true;
		
		$scope.InscriptionRepetiteurDTO.tableauMatieres = nouveau_tableau_matiere;
		
		RepetiteurResource.create($scope.InscriptionRepetiteurDTO).then(function(data){
    		console.log("success total");
    		$location.path("#/");
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
	};
	
	$scope.cancel = function() {
		$location.path("#/");
	}
    
    
    $scope.cocheOupas = function(result) {

		var trouve = false;
		console.log(nouveau_tableau_matiere.length);
		
		if (nouveau_tableau_matiere.length == 0){
			nouveau_tableau_matiere.push(result);
		}
		else{
			for(var i = 0; i < nouveau_tableau_matiere.length ; i++){
				if(result.id == nouveau_tableau_matiere[i].id){
					trouve = true;
				}
			}
			
			if(trouve == true){
				var indice = result.id - 1 ;
				nouveau_tableau_matiere.splice(indice,1);
				console.log(nouveau_tableau_matiere.length);
			}
			else{
				nouveau_tableau_matiere.push(result);
			}
			
		}
		
			for(var i = 0; i < nouveau_tableau_matiere.length ; i++){
				console.log(nouveau_tableau_matiere[i].intitule);
			}
			
			console.log(nouveau_tableau_matiere.length);
	};
    
	
	/* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});