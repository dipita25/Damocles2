

angular.module('damocles').controller('EditRepetition_RepetiteurController', function($scope,$window,$rootScope, $routeParams, $location, flash, Repetition_RepetiteurResource , RepetitionResource, RepetiteurResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.repetition_Repetiteur = new Repetition_RepetiteurResource(self.original);
            RepetitionResource.queryAll(function(items) {
                $scope.repetitionSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.repetition_Repetiteur.repetition && item.id == $scope.repetition_Repetiteur.repetition.id) {
                        $scope.repetitionSelection = labelObject;
                        $scope.repetition_Repetiteur.repetition = wrappedObject;
                        self.original.repetition = $scope.repetition_Repetiteur.repetition;
                    }
                    return labelObject;
                });
            });
            RepetiteurResource.queryAll(function(items) {
                $scope.repetiteurSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.repetition_Repetiteur.repetiteur && item.id == $scope.repetition_Repetiteur.repetiteur.id) {
                        $scope.repetiteurSelection = labelObject;
                        $scope.repetition_Repetiteur.repetiteur = wrappedObject;
                        self.original.repetiteur = $scope.repetition_Repetiteur.repetiteur;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetition_Repetiteur could not be found.'});
            $location.path("/Repetition_Repetiteurs");
        };
        Repetition_RepetiteurResource.get({Repetition_RepetiteurId:$routeParams.Repetition_RepetiteurId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.repetition_Repetiteur);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The repetition_Repetiteur was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.repetition_Repetiteur.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Repetition_Repetiteurs");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetition_Repetiteur was deleted.'});
            $location.path("/Repetition_Repetiteurs");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.repetition_Repetiteur.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("repetitionSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.repetition_Repetiteur.repetition = {};
            $scope.repetition_Repetiteur.repetition.id = selection.value;
        }
    });
    $scope.$watch("repetiteurSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.repetition_Repetiteur.repetiteur = {};
            $scope.repetition_Repetiteur.repetiteur.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});