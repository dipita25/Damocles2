

angular.module('damocles').controller('EditMatiere_RepetitionController', function($scope,$window,$rootScope, $routeParams, $location, flash, Matiere_RepetitionResource , MatiereResource, RepetitionResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.matiere_Repetition = new Matiere_RepetitionResource(self.original);
            MatiereResource.queryAll(function(items) {
                $scope.matiereSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.matiere_Repetition.matiere && item.id == $scope.matiere_Repetition.matiere.id) {
                        $scope.matiereSelection = labelObject;
                        $scope.matiere_Repetition.matiere = wrappedObject;
                        self.original.matiere = $scope.matiere_Repetition.matiere;
                    }
                    return labelObject;
                });
            });
            RepetitionResource.queryAll(function(items) {
                $scope.repetitionSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.matiere_Repetition.repetition && item.id == $scope.matiere_Repetition.repetition.id) {
                        $scope.repetitionSelection = labelObject;
                        $scope.matiere_Repetition.repetition = wrappedObject;
                        self.original.repetition = $scope.matiere_Repetition.repetition;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The matiere_Repetition could not be found.'});
            $location.path("/Matiere_Repetitions");
        };
        Matiere_RepetitionResource.get({Matiere_RepetitionId:$routeParams.Matiere_RepetitionId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.matiere_Repetition);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The matiere_Repetition was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.matiere_Repetition.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Matiere_Repetitions");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The matiere_Repetition was deleted.'});
            $location.path("/Matiere_Repetitions");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.matiere_Repetition.$remove(successCallback, errorCallback);
    };
    
    $scope.$watch("matiereSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.matiere_Repetition.matiere = {};
            $scope.matiere_Repetition.matiere.id = selection.value;
        }
    });
    $scope.$watch("repetitionSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.matiere_Repetition.repetition = {};
            $scope.matiere_Repetition.repetition.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});