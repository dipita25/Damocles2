

angular.module('damocles').controller('EditUtilisateurController', function($scope,$window,$rootScope, $routeParams, $location, flash, UtilisateurResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        var successCallback = function(data){
            self.original = data;
            $scope.utilisateur = new UtilisateurResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The utilisateur could not be found.'});
            $location.path("/Utilisateurs");
        };
        UtilisateurResource.get({UtilisateurId:$routeParams.UtilisateurId}, successCallback, errorCallback);
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.utilisateur);
    };

    $scope.save = function() {
        var successCallback = function(){
            flash.setMessage({'type':'success','text':'The utilisateur was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.utilisateur.$update(successCallback, errorCallback);
    };

    $scope.cancel = function() {
        $location.path("/Utilisateurs");
    };

    $scope.remove = function() {
        var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The utilisateur was deleted.'});
            $location.path("/Utilisateurs");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.utilisateur.$remove(successCallback, errorCallback);
    };
    
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});