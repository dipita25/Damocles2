

angular.module('damocles').controller('InscriptionParentController', 
		function($scope, $window,$routeParams, $location, flash, $filter, UtilisateurResource,$rootScope, ParentResource, MatiereResource, 
				ApprenantResource, RepetitionResource ,RepetiteurResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.ApprenantDTO = $scope.ApprenantDTO || {};
    $scope.ParentDTO = $scope.ParentDTO || {};
    $scope.MatiereDTO = $scope.MatiereDTO || {};
    $scope.RepetitionDTO = $scope.RepetitionDTO || {};
    $scope.UtilisateurDTO = $scope.UtilisateurDTO || {};
    $scope.InscriptionParentDTO = $scope.InscriptionParentDTO || {};
    $scope.searchResults = [];
    $scope.valeur = $scope.valeur;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		//$location.path("#/");
    }
    
    /* pour l'indice de l'apprenant dans le tableau d'apprenants qui sera enregistré*/
    $scope.numero_apprenant = $scope.numero_apprenant;
    $scope.affiche = $scope.affiche;
    
    $scope.afficher = false;
    var longueur_tableau = 0;
    var nouveau_tableau_matiere_repetition = [];
	$scope.numero_apprenant = 0;
	$scope.affiche = $scope.numero_apprenant + 1;
	
	/*variable servant à stocker les apprenants avant enregistrement*/
	var apprenants = [];
	
	var matieres_etudiant = [];
	
	/*variable servant à stocker les matieres par etudiant avant enregistrement*/
	var nouveau_tableau_matiere_apprenant = [];
    
    $scope.suivant = function(){
        $rootScope.showSpinner = true;
		$scope.afficher = true;
        $scope.searchResults = MatiereResource.queryAll().then(function(data){
    		console.log("success total");
    		$scope.searchResults = data;
    		console.log($scope.searchResults);
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };
    
    $scope.cancel = function() {
		$location.path("#/");
	}
    
    
    $scope.terminer = function() {
        $rootScope.showSpinner = true;
		
		apprenants.push($scope.ApprenantDTO);
		matieres_etudiant.push(nouveau_tableau_matiere_apprenant);
		$scope.InscriptionParentDTO.apprenants = apprenants;
		$scope.InscriptionParentDTO.tableauMatieres_apprenant = matieres_etudiant;
		
		ParentResource.create($scope.InscriptionParentDTO).then(function(data){
    		console.log("success total");
    		
    		RepetiteurResource.addToRepetition(data).then(function(data2){
    			console.log(data2);
    			console.log(data2.length);
    			},function(error){
    				console.log(error);
    		});
    		/*****relancer l'attribution automatiquement apres 01 minutes****/
    		setInterval(function(){
    			RepetiteurResource.addToRepetition(data).then(function(data3){
        			console.log(data3);
        			},function(error){
        				console.log(error);
        		});
    		},60000);
    		/*****fin*********/
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
	};
	
	$scope.continuer = function() {
		
		apprenants.push($scope.ApprenantDTO);
		matieres_etudiant.push(nouveau_tableau_matiere_apprenant);
		
		for(var i=0;i < apprenants.length ; i++){
			console.log(apprenants[i].nom);
		}
		/*$scope.InscriptionParentDTO.tableauMatieres = nouveau_tableau_matiere_repetition;*/
		$scope.InscriptionParentDTO.tableauMatieres_apprenant = matieres_etudiant;
		
		/*vider les champs*/
		$scope.ApprenantDTO = {};

		$scope.numero_apprenant = $scope.numero_apprenant + 1 ;
		$scope.affiche = $scope.numero_apprenant + 1 ;
		
		/* on décoche toutes les checkbox */
		var bloc_matiere = document.getElementById("matieres");
		var checkboxs = bloc_matiere.getElementsByTagName("input");
		
		for(var i = 0; i < checkboxs.length ; i++){
			
			checkboxs[i].checked = false;
		}
		/*fin decochement des checkbox*/

		/* on vide le tableau des matieres pour qu'il puisse recevoir les matieres de l'apprenant suivant */
		nouveau_tableau_matiere_apprenant = [];
		for (var i=0;i < matieres_etudiant.length; i++){
			var numero = i +1 ;
			console.log("matiere de etudiant "+ numero);
			for(var j=0;j < matieres_etudiant[i].length;j++){
				console.log( matieres_etudiant[i][j].intitule);
			}
		}

	};
	
	$scope.cocheOupas = function(result) {

		var trouve = false;
		console.log(nouveau_tableau_matiere_apprenant.length);
		
		if (nouveau_tableau_matiere_apprenant.length == 0){
			nouveau_tableau_matiere_apprenant.push(result);
		}
		else{
			for(var i = 0; i < nouveau_tableau_matiere_apprenant.length ; i++){
				if(result.id == nouveau_tableau_matiere_apprenant[i].id){

					nouveau_tableau_matiere_apprenant.splice(i,1);
					trouve = true;
				}
			}
			
			if(trouve == false){
				nouveau_tableau_matiere_apprenant.push(result);
				console.log(nouveau_tableau_matiere_apprenant.length);
			}
			/*else{
				nouveau_tableau_matiere_apprenant.push(result);
			}*/
			
		}
		
			for(var i = 0; i < nouveau_tableau_matiere_apprenant.length ; i++){
				console.log(nouveau_tableau_matiere_apprenant[i].intitule);
			}
			
			console.log(nouveau_tableau_matiere_apprenant.length);
	};
	
	
	/* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});