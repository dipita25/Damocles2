/*angular.module('damocles').factory('ApprenantsParentResource', function($resource){
	var resource = $resource('rest/apprenantsParent',null,{'lister':{method:'POST',isArray:true}});
	
    return resource;
});
*/

function ApprenantsParentResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.lister = function (utilisateurDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/apprenantsParent",
            data: utilisateurDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}

angular.module('damocles').factory('ApprenantsParentResource', ['$q', '$http', '$rootScope', '$timeout', ApprenantsParentResource]);