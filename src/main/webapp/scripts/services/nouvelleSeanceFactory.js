/*angular.module('damocles').factory('NouvelleSeanceResource', function($resource){
	var resource = $resource('rest/newSeance',null,{'nouveau':{method:'POST',isArray:false}});
	
    return resource;
});*/

function NouvelleSeanceResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.nouveau = function (seanceDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/newSeance",
            data: seanceDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}

angular.module('damocles').factory('NouvelleSeanceResource', ['$q', '$http', '$rootScope', '$timeout', NouvelleSeanceResource]);