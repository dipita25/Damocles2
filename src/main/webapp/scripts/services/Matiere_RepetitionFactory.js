angular.module('damocles').factory('Matiere_RepetitionResource', function($resource){
    var resource = $resource('rest/matiere_repetitions/:Matiere_RepetitionId',{Matiere_RepetitionId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});