angular.module('damocles').factory('Repetition_ApprenantResource', function($resource){
    var resource = $resource('rest/repetition_apprenants/:Repetition_ApprenantId',{Repetition_ApprenantId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});