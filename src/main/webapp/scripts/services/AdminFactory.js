angular.module('damocles').factory('AdminResource', function($resource){
    var resource = $resource('rest/admins/:AdminId',{AdminId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});