angular.module('damocles').factory('typeResource', function($resource){
    var resource = $resource('rest/types/:typeId',{typeId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});