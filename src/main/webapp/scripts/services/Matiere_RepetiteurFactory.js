angular.module('damocles').factory('Matiere_RepetiteurResource', function($resource){
    var resource = $resource('rest/matiere_repetiteurs/:Matiere_RepetiteurId',{Matiere_RepetiteurId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});