/*angular.module('damocles').factory('FeedbackRepetiteurFactoryResource', function($resource){
	var resource = $resource('rest/feedbackRepetiteur',null,{'listerSeance':{method:'POST',isArray:true}});
	
    return resource;
});*/


function FeedbackRepetiteurFactoryResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.enregistrer = function (feedbackDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/feedbackRepetiteur",
            data: feedbackDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    service.listerSeance = function (utilisateurDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/feedbackRepetiteur",
            data: utilisateurDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}


angular.module('damocles').factory('FeedbackRepetiteurFactoryResource', ['$q', '$http', '$rootScope', '$timeout', FeedbackRepetiteurFactoryResource]);