/*angular.module('damocles').factory('Nbre_seanceResource', function($resource){
	var resource = $resource('rest/nbre_seance',null,{'nbre':{method:'POST',isArray:true}});
	
    return resource;
});*/


function Nbre_seanceResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.nbre = function (ApprenantDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/nbre_seance",
            data: ApprenantDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}

angular.module('damocles').factory('Nbre_seanceResource', ['$q', '$http', '$rootScope', '$timeout', Nbre_seanceResource]);