angular.module('damocles').factory('Repetition_RepetiteurResource', function($resource){
    var resource = $resource('rest/repetition_repetiteurs/:Repetition_RepetiteurId',{Repetition_RepetiteurId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});