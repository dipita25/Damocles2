/*angular.module('damocles').factory('Matiere_PlanningResource', function($resource){
	var resource = $resource('rest/mesMatieres',null,{'mesMatieres':{method:'POST',isArray:true}});
	
    return resource;
});*/




function Matiere_PlanningResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.mesMatieres = function (ApprenantDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/mesMatieres",
            data: ApprenantDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}

angular.module('damocles').factory('Matiere_PlanningResource', ['$q', '$http', '$rootScope', '$timeout', Matiere_PlanningResource]);