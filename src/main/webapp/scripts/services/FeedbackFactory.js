/*angular.module('damocles').factory('FeedbackResource', function($resource){
    var resource = $resource('rest/feedbacks/:FeedbackId',{FeedbackId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});*/

function FeedbackResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.queryAll = function () {
        var d = $q.defer();
        var url = "rest/feedbacks";
        $http.get(url).then(function (response) {
            var items = response.data;
            d.resolve(items);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.query = function (id) {
    	var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/feedbacks/get",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.supprimer = function (id) {
    	var d = $q.defer();
        $http({
            method: 'DELETE',
            url: "rest/feedbacks/delete",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.update = function (entity) {
    	var d = $q.defer();
        $http({
            method: 'PUT',
            url: "rest/feedbacks/update",
            data: entity,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    return service;
}


angular.module('damocles').factory('FeedbackResource', ['$q', '$http', '$rootScope', '$timeout', FeedbackResource]);