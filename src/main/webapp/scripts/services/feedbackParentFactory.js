/*angular.module('damocles').factory('FeedbackParentFactoryResource', function($resource){
	var resource = $resource('rest/feedbackParent',null,{'listerSeance':{method:'POST',isArray:true}});
	
    return resource;
});*/

function FeedbackParentFactoryResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.enregistrer = function (feedbackDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/feedbackParent",
            data: feedbackDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    service.listerSeance = function (utilisateurDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/feedbackParent",
            data: utilisateurDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}


angular.module('damocles').factory('FeedbackParentFactoryResource', ['$q', '$http', '$rootScope', '$timeout', FeedbackParentFactoryResource]);