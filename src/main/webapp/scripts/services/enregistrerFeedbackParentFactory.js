/*angular.module('damocles').factory('EnregistrerFeedbackParentResource', function($resource){
	var resource = $resource('rest/enregistrerFeedbackParent',null,{'enregistrer':{method:'POST',isArray:false}});
	
    return resource;
});
*/
function EnregistrerFeedbackParentResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.enregistrer = function (feedbackDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/enregistrerFeedbackParent",
            data: feedbackDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}




angular.module('damocles').factory('EnregistrerFeedbackParentResource', ['$q', '$http', '$rootScope', '$timeout', EnregistrerFeedbackParentResource]);