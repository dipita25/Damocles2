/*angular.module('damocles').factory('SeanceEtudiantResource', function($resource){
	var resource = $resource('rest/SeanceEtudiant',null,{'detail':{method:'POST',isArray:true}});
	
    return resource;
});*/


function SeanceEtudiantResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.detail = function (ApprenantDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/SeanceEtudiant",
            data: ApprenantDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}

angular.module('damocles').factory('SeanceEtudiantResource', ['$q', '$http', '$rootScope', '$timeout', SeanceEtudiantResource]);