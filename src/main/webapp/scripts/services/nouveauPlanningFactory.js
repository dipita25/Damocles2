/*angular.module('damocles').factory('NouveauPlanningResource', function($resource){
	var resource = $resource('rest/mesApprenants',null,{'mesApprenants':{method:'POST',isArray:true}});
	
    return resource;
});*/

function NouveauPlanningResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.mesApprenants = function (utilisateurDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/mesApprenants",
            data: utilisateurDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}

angular.module('damocles').factory('NouveauPlanningResource', ['$q', '$http', '$rootScope', '$timeout', NouveauPlanningResource]);