'use strict';

angular.module('damocles',['ngRoute','ngResource','angularSpinner'])
  .config(['$routeProvider', function($routeProvider) {
    $routeProvider
      .when('/',{templateUrl:'views/landing.html',controller:'LandingPageController'})
      .when('/login',{templateUrl:'views/login.html',controller:'LoginController'})
      .when('/deconnexion',{templateUrl:'views/deconnexion.html',controller:'DeconnexionController'})
      .when('/Admins',{templateUrl:'views/Admin/search.html',controller:'SearchAdminController'})
      .when('/Admins/new',{templateUrl:'views/Admin/detail.html',controller:'NewAdminController'})
      .when('/Admins/edit/:AdminId',{templateUrl:'views/Admin/detail.html',controller:'EditAdminController'})
      .when('/Apprenants',{templateUrl:'views/Apprenant/search.html',controller:'SearchApprenantController'})
      .when('/Apprenants/new',{templateUrl:'views/Apprenant/detail.html',controller:'NewApprenantController'})
      .when('/Apprenants/edit/:ApprenantId',{templateUrl:'views/Apprenant/detail.html',controller:'EditApprenantController'})
      .when('/Feedbacks',{templateUrl:'views/Feedback/search.html',controller:'SearchFeedbackController'})
      .when('/Feedbacks/new',{templateUrl:'views/Feedback/detail.html',controller:'NewFeedbackController'})
      .when('/Feedbacks/edit/:FeedbackId',{templateUrl:'views/Feedback/detail.html',controller:'EditFeedbackController'})
      .when('/Matieres',{templateUrl:'views/Matiere/search.html',controller:'SearchMatiereController'})
      .when('/Matieres/new',{templateUrl:'views/Matiere/detail.html',controller:'NewMatiereController'})
      .when('/Matieres/edit/:MatiereId',{templateUrl:'views/Matiere/detail.html',controller:'EditMatiereController'})
      .when('/Matiere_Repetiteurs',{templateUrl:'views/Matiere_Repetiteur/search.html',controller:'SearchMatiere_RepetiteurController'})
      .when('/Matiere_Repetiteurs/new',{templateUrl:'views/Matiere_Repetiteur/detail.html',controller:'NewMatiere_RepetiteurController'})
      .when('/Matiere_Repetiteurs/edit/:Matiere_RepetiteurId',{templateUrl:'views/Matiere_Repetiteur/detail.html',controller:'EditMatiere_RepetiteurController'})
      .when('/Matiere_Repetitions',{templateUrl:'views/Matiere_Repetition/search.html',controller:'SearchMatiere_RepetitionController'})
      .when('/Matiere_Repetitions/new',{templateUrl:'views/Matiere_Repetition/detail.html',controller:'NewMatiere_RepetitionController'})
      .when('/Matiere_Repetitions/edit/:Matiere_RepetitionId',{templateUrl:'views/Matiere_Repetition/detail.html',controller:'EditMatiere_RepetitionController'})
      .when('/Parents',{templateUrl:'views/Parent/search.html',controller:'SearchParentController'})
      .when('/Parent/',{templateUrl:'views/Parent/compteParent.html',controller:'CompteParentController'})
      .when('/Parent/feedback',{templateUrl:'views/Parent/feedbackParent.html',controller:'FeedbackParentController'})
      .when('/inscription/parent',{templateUrl:'views/Parent/inscriptionParent.html',controller:'InscriptionParentController',authorized: true})
      .when('/Parent/planning',{templateUrl:'views/Parent/planningParent.html',controller:'PlanningParentController'})
      .when('/Parents/new',{templateUrl:'views/Parent/detail.html',controller:'NewParentController'})
      .when('/Parents/edit/:ParentId',{templateUrl:'views/Parent/detail.html',controller:'EditParentController'})
      .when('/Repetiteurs',{templateUrl:'views/Repetiteur/search.html',controller:'SearchRepetiteurController'})
      .when('/inscription/repetiteur',{templateUrl:'views/Repetiteur/inscriptionRepetiteur.html',controller:'InscriptionRepetiteurController',authorized: true})
      .when('/Repetiteur',{templateUrl:'views/Repetiteur/compteRepetiteur.html',controller:'CompteRepetiteurController'})
      .when('/Repetiteur/apprenants',{templateUrl:'views/Repetiteur/apprenantsRepetiteur.html',controller:'ApprenantsRepetiteurController'})
      .when('/Repetiteur/feedback',{templateUrl:'views/Repetiteur/feedbackRepetiteur.html',controller:'FeedbackRepetiteurController'})
      .when('/Repetiteur/planning',{templateUrl:'views/Repetiteur/planningRepetiteur.html',controller:'PlanningRepetiteurController'})
      .when('/Repetiteur/planning/nouveau',{templateUrl:'views/Repetiteur/nouveauPlanningRepetiteur.html',controller:'NouveauPlanningRepetiteurController'})
      .when('/Repetiteurs/new',{templateUrl:'views/Repetiteur/detail.html',controller:'NewRepetiteurController'})
      .when('/Repetiteurs/edit/:RepetiteurId',{templateUrl:'views/Repetiteur/detail.html',controller:'EditRepetiteurController'})
      .when('/Repetitions',{templateUrl:'views/Repetition/search.html',controller:'SearchRepetitionController'})
      .when('/Repetitions/new',{templateUrl:'views/Repetition/detail.html',controller:'NewRepetitionController'})
      .when('/Repetitions/edit/:RepetitionId',{templateUrl:'views/Repetition/detail.html',controller:'EditRepetitionController'})
      .when('/Repetition_Apprenants',{templateUrl:'views/Repetition_Apprenant/search.html',controller:'SearchRepetition_ApprenantController'})
      .when('/Repetition_Apprenants/new',{templateUrl:'views/Repetition_Apprenant/detail.html',controller:'NewRepetition_ApprenantController'})
      .when('/Repetition_Apprenants/edit/:Repetition_ApprenantId',{templateUrl:'views/Repetition_Apprenant/detail.html',controller:'EditRepetition_ApprenantController'})
      .when('/Repetition_Repetiteurs',{templateUrl:'views/Repetition_Repetiteur/search.html',controller:'SearchRepetition_RepetiteurController'})
      .when('/Repetition_Repetiteurs/new',{templateUrl:'views/Repetition_Repetiteur/detail.html',controller:'NewRepetition_RepetiteurController'})
      .when('/Repetition_Repetiteurs/edit/:Repetition_RepetiteurId',{templateUrl:'views/Repetition_Repetiteur/detail.html',controller:'EditRepetition_RepetiteurController'})
      .when('/Seances',{templateUrl:'views/Seance/search.html',controller:'SearchSeanceController'})
      .when('/Seances/new',{templateUrl:'views/Seance/detail.html',controller:'NewSeanceController'})
      .when('/Seances/edit/:SeanceId',{templateUrl:'views/Seance/detail.html',controller:'EditSeanceController'})
      .when('/Utilisateurs',{templateUrl:'views/Utilisateur/search.html',controller:'SearchUtilisateurController'})
      .when('/Utilisateurs/new',{templateUrl:'views/Utilisateur/detail.html',controller:'NewUtilisateurController'})
      .when('/Utilisateurs/edit/:UtilisateurId',{templateUrl:'views/Utilisateur/detail.html',controller:'EditUtilisateurController'})
      .otherwise({
        redirectTo: '/'
      });
  }])
  .controller('LandingPageController', function LandingPageController($scope,$window, $routeParams, $location, flash, UtilisateurResource, LoginDTOResource,$rootScope) {
	  var self = this;
	    $scope.disabled = false;
	    $scope.$location = $location;
	    $scope.loginDTO = $scope.loginDTO || {};
	    $rootScope.showSpinner = false;
	    
	    
	    
	    $scope.save = function() {
	    	
	    	$rootScope.showSpinner = true;
	    	
	        LoginDTOResource.login($scope.loginDTO).then(function(data){
	        	
	        	console.log(data);
	        	
	        	if(data.type_utilisateur == "admin"){
	            	
	            	/* sauvegarde des infos du user connecté dans le localStorage*/
	            	$window.localStorage.setItem("id_personne",data.id_personne);
	            	$window.localStorage.setItem("type_utilisateur",data.type_utilisateur);
	    			$rootScope.authenticated = true;
	    			$rootScope.parent = false;
	    			$rootScope.repetiteur = false;
	    			$rootScope.admin = true;
	    			
	    			var user = $window.localStorage.getItem("id_personne");
	    			var type = $window.localStorage.getItem("type_utilisateur");
	    			
	    			console.log($rootScope.showSpinner);
	            	//$rootScope.showSpinner = true;
	    			flash.setMessage({'type': 'success', 'text': 'connexion reussie'});
	            	/* redirection vers sa page personnelle*/
	            	$location.path('/Repetitions');
	            	//$rootScope.showSpinner = false;
	            }
	            
	            else if(data.type_utilisateur == "parent"){
	            	
	            	/* sauvegarde des infos du user connecté dans le localStorage*/
	            	$window.localStorage.setItem("id_personne",data.id_personne);
	            	$window.localStorage.setItem("type_utilisateur",data.type_utilisateur);
	    			$rootScope.authenticated = true;
	    			$rootScope.admin = false;
	    			$rootScope.repetiteur = false;
	    			$rootScope.parent = true;
	            	
	    			
	    			flash.setMessage({'type': 'success', 'text': 'connexion reussie'});
	            	/* redirection vers sa page personnelle*/
	            	$location.path('/Parent/planning');
	            }
	            
	            else if(data.type_utilisateur == "repetiteur"){
	            	
	        	   /* sauvegarde des infos du user connecté dans le localStorage*/
	           		$window.localStorage.setItem("id_personne",data.id_personne);
	           		$window.localStorage.setItem("type_utilisateur",data.type_utilisateur);
	    			$rootScope.authenticated = true;
	    			$rootScope.parent = false;
	    			$rootScope.admin = false;
	    			$rootScope.repetiteur = true;
	            	
	    			
	    			flash.setMessage({'type': 'success', 'text': 'connexion reussie'});
	            	/* redirection vers sa page personnelle*/
	            	$location.path('/Repetiteur/planning');
	            }
	           
	            else if(data.type_utilisateur == null){
	            	
	            	flash.setMessage({'type': 'error', 'text': 'login ou mot de passe incorrect'});
	            }
	        	
	        },function(error){
	        	
	        });
	    };
  })
  .controller('NavController', function NavController($scope, $location) {
    $scope.matchesRoute = function(route) {
        var path = $location.path();
        return (path === ("/" + route) || path.indexOf("/" + route + "/") == 0);
    };
  })
  
  .run(function ($rootScope) {
    $rootScope.$on("$routeChangeStart", function () {
      //if user is not logged in
      if($rootScope.authenticated == false){ 
    	  console.log("veuillez vous connecter pour acceder a cette page");
    	  console.log($rootScope.authenticated);
      }
    })
  });
  ;
