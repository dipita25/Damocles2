

angular.module('damocles').controller('EditRepetiteurController', function($scope,$window,$rootScope, $routeParams, $location, flash, RepetiteurResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(data){
            self.original = data;
            $scope.repetiteur = new RepetiteurResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetiteur could not be found.'});
            $location.path("/Repetiteurs");
        };
        RepetiteurResource.get({RepetiteurId:$routeParams.RepetiteurId}, successCallback, errorCallback);*/
        var RepetiteurId = {RepetiteurId:$routeParams.RepetiteurId};
        RepetiteurResource.query(RepetiteurId.RepetiteurId).then(function(data){
    		console.log("success total");
    		self.original = data;
            $scope.repetiteur = data;
		
			},function(error){
				console.log("erreur");
				$location.path("/repetiteurs");
		});
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.repetiteur);
    };

    $scope.save = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(){
            flash.setMessage({'type':'success','text':'The repetiteur was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.repetiteur.$update(successCallback, errorCallback);*/
    	RepetiteurResource.update($scope.repetiteur).then(function(data){
    		console.log("success total");
    		$scope.get();
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };

    $scope.cancel = function() {
        $location.path("/Repetiteurs");
    };

    $scope.remove = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetiteur was deleted.'});
            $location.path("/Repetiteurs");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.repetiteur.$remove(successCallback, errorCallback);*/
        var RepetiteurId = {RepetiteurId:$routeParams.RepetiteurId};
        //MatiereResource.get({MatiereId:$routeParams.MatiereId}, successCallback, errorCallback);
        RepetiteurResource.supprimer(RepetiteurId.RepetiteurId).then(function(data){
    		console.log("success total");
    		$location.path("/Repetiteurs");
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };
    
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});