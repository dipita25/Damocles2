

angular.module('damocles').controller('PlanningParentController', function($scope, $window,$routeParams,$window, $location, flash,
		UtilisateurResource,Matiere_PlanningResource ,NouveauPlanningResource,SeanceEtudiantResource,Nbre_seanceResource,ApprenantsParentResource,$rootScope) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.activeMenu = [0];
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }

    $scope.utilisateurDTO = $scope.utilisateurDTO || {};
    $scope.ApprenantsResults = [];
    $scope.searchMatieresResults = [];
    $scope.seancesResults = [];
    $scope.searchApprenantsResults = [];
    $scope.ApprenantDTO = $scope.ApprenantDTO || {};
    
    $scope.utilisateurDTO.id_personne = $window.localStorage.getItem("id_personne");
    
    /* fonction de recherche d'apprenants en fonction du user loggé*/
    var recherche = function(){
    	$rootScope.showSpinner = true;
    	
    	ApprenantsParentResource.lister($scope.utilisateurDTO).then(function(data){
    		console.log("success total");
    		$scope.ApprenantsResults = data;
		
			},function(error){
				console.log("erreur");
    	
		});
    }
    
    /* en fonction de l'apprenant selectionné,on affiche les matieres pour lesquelles il recoit des repetitions*/
    $scope.ajusterMatiere = function(resultat){

    	$rootScope.showSpinner = true;
    	
    	$scope.activeMenu = resultat;
    	
    	$scope.ApprenantDTO.id = resultat.id;
    	
    	Matiere_PlanningResource.mesMatieres($scope.ApprenantDTO).then(function(data){
    		console.log("success total");
    		$scope.searchMatieresResults = data;
		
			},function(error){
				console.log("erreur");
		});
    	
    	SeanceEtudiantResource.detail($scope.ApprenantDTO).then(function(data){
    		console.log("success total");
    		$scope.SeancesResults = data;
		
			},function(error){
				console.log("erreur");
		});
    	
    	Nbre_seanceResource.nbre($scope.ApprenantDTO).then(function(data){
    		console.log("success total");
    		$scope.nbre_seance = data[0];
    		$scope.seances_effectuees = data[1];
    		$scope.seances_restantes = data[2];
		
			},function(error){
				console.log("erreur");
		});
    }
    
    /*l'appel de cette fonction permet de rechercher et remplir le tableau d'apprenants uniquement avec ceux qui concernent l'utilisateur loggé*/
    recherche();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
    
});