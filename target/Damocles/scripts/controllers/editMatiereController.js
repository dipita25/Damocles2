

angular.module('damocles').controller('EditMatiereController', function($scope,$window,$rootScope, $routeParams, $location, flash, MatiereResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(data){
            self.original = data;
            $scope.matiere = new MatiereResource(self.original);
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The matiere could not be found.'});
            $location.path("/Matieres");
        };*/
    	var MatiereId = {MatiereId:$routeParams.MatiereId};
        //MatiereResource.get({MatiereId:$routeParams.MatiereId}, successCallback, errorCallback);
    	MatiereResource.query(MatiereId.MatiereId).then(function(data){
    		console.log("success total");
    		self.original = data;
            $scope.matiere = data;
		
			},function(error){
				console.log("erreur");
				$location.path("/matieres");
		});
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.matiere);
    };

    $scope.save = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(){
            flash.setMessage({'type':'success','text':'The matiere was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.matiere.$update(successCallback, errorCallback);*/
        MatiereResource.update($scope.matiere).then(function(data){
    		console.log("success total");
    		$scope.get();
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };

    $scope.cancel = function() {
        $location.path("/Matieres");
    };

    $scope.remove = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The matiere was deleted.'});
            $location.path("/Matieres");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.matiere.$remove(successCallback, errorCallback);*/
    	
    	var MatiereId = {MatiereId:$routeParams.MatiereId};
        //MatiereResource.get({MatiereId:$routeParams.MatiereId}, successCallback, errorCallback);
    	MatiereResource.supprimer(MatiereId.MatiereId).then(function(data){
    		console.log("success total");
    		$location.path("/Matieres");
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };
    
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});