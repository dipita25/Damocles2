

angular.module('damocles').controller('LoginController','Flash', function($scope,$window,Flash, $routeParams, $location, flash, UtilisateurResource, LoginDTOResource,$rootScope ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.loginDTO = $scope.loginDTO || {};
    $rootScope.showSpinner = false;
    $scope.showAlert = false;
    
    
    
    $scope.save = function() {
    	
    	$rootScope.showSpinner = true;
    	
        LoginDTOResource.login($scope.loginDTO).then(function(data){
        	
        	console.log(data);
        	
        	if(data.type_utilisateur == "admin"){
            	
            	/* sauvegarde des infos du user connecté dans le localStorage*/
            	$window.localStorage.setItem("id_personne",data.id_personne);
            	$window.localStorage.setItem("type_utilisateur",data.type_utilisateur);
    			$rootScope.authenticated = true;
    			$rootScope.parent = false;
    			$rootScope.repetiteur = false;
    			$rootScope.admin = true;
    			
    			var user = $window.localStorage.getItem("id_personne");
    			var type = $window.localStorage.getItem("type_utilisateur");
    			
    			console.log($rootScope.showSpinner);
            	$rootScope.showSpinner = true;
    			flash.setMessage({'type': 'success', 'text': 'connexion reussie'});
            	/* redirection vers sa page personnelle*/
            	$location.path('/Repetitions');
            	$rootScope.showSpinner = false;
            }
            
            else if(data.type_utilisateur == "parent"){
            	
            	/* sauvegarde des infos du user connecté dans le localStorage*/
            	$window.localStorage.setItem("id_personne",data.id_personne);
            	$window.localStorage.setItem("type_utilisateur",data.type_utilisateur);
    			$rootScope.authenticated = true;
    			$rootScope.admin = false;
    			$rootScope.repetiteur = false;
    			$rootScope.parent = true;
            	
    			$rootScope.showSpinner = true;
    			console.log($rootScope.showSpinner);
    			flash.setMessage({'type': 'success', 'text': 'connexion reussie'});
            	/* redirection vers sa page personnelle*/
            	$location.path('/Parent/planning');
    			console.log("parent");
            }
            
            else if(data.type_utilisateur == "repetiteur"){
            	
        	   /* sauvegarde des infos du user connecté dans le localStorage*/
           		$window.localStorage.setItem("id_personne",data.id_personne);
           		$window.localStorage.setItem("type_utilisateur",data.type_utilisateur);
    			$rootScope.authenticated = true;
    			$rootScope.parent = false;
    			$rootScope.admin = false;
    			$rootScope.repetiteur = true;
            	
    			$rootScope.showSpinner = true;
    			flash.setMessage({'type': 'success', 'text': 'connexion reussie'});
            	/* redirection vers sa page personnelle*/
    			console.log("repetiteur");
            	$location.path('/Repetiteur/planning');
            }
           
            else if(data.type_utilisateur == null){
            	
            	flash.setMessage({'type': 'error', 'text': 'login ou mot de passe incorrect'});
                $scope.showAlert = true;
                Flash.create('success', message, 0, {class: 'custom-class', id: 'custom-id'}, true);
                $scope.myCallback = function(flash){
                	console.log('call back flash');
                };
            }
        	
        },function(error){
        	
        });
    };
    
    
});