

angular.module('damocles').controller('EditRepetitionController', function($scope,$window,$rootScope, $routeParams, $location, flash, RepetitionResource , ParentResource) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.get = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function(data){
            self.original = data;
            $scope.repetition = new RepetitionResource(self.original);
            ParentResource.queryAll(function(items) {
                $scope.parentSelectionList = $.map(items, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.repetition.parent && item.id == $scope.repetition.parent.id) {
                        $scope.parentSelection = labelObject;
                        $scope.repetition.parent = wrappedObject;
                        self.original.parent = $scope.repetition.parent;
                    }
                    return labelObject;
                });
            });
        };
        var errorCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetition could not be found.'});
            $location.path("/Repetitions");
        };
        RepetitionResource.get({RepetitionId:$routeParams.RepetitionId}, successCallback, errorCallback);*/
    	
    	var RepetitionId = {RepetitionId:$routeParams.RepetitionId};
        //MatiereResource.get({MatiereId:$routeParams.MatiereId}, successCallback, errorCallback);
    	RepetitionResource.query(RepetitionId.RepetitionId).then(function(data){
    		console.log("success total");
    		self.original = data;
            $scope.repetition = data;
            console.log($scope.repetition);
            
            ParentResource.queryAll().then(function(data){
        		console.log("success total");
        		$scope.parentSelectionList = $.map(data, function(item) {
                    var wrappedObject = {
                        id : item.id
                    };
                    var labelObject = {
                        value : item.id,
                        text : item.id
                    };
                    if($scope.repetition.parent && item.id == $scope.repetition.parent.id) {
                        $scope.parentSelection = labelObject;
                        $scope.repetition.parent = wrappedObject;
                        self.original.parent = $scope.repetition.parent;
                    }
                    return labelObject;
                });
    		
    			},function(error){
    				console.log("erreur");
    		});
		
			},function(error){
				console.log("erreur");
				$location.path("/Repetitions");
		});
    };

    $scope.isClean = function() {
        return angular.equals(self.original, $scope.repetition);
    };

    $scope.save = function() {
        $rootScope.showSpinner = true;
        
        /*var successCallback = function(){
            flash.setMessage({'type':'success','text':'The repetition was updated successfully.'}, true);
            $scope.get();
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        $scope.repetition.$update(successCallback, errorCallback);*/
        RepetitionResource.update($scope.repetition).then(function(data){
    		console.log("success total");
    		$scope.get();
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };

    $scope.cancel = function() {
        $location.path("/Repetitions");
    };

    $scope.remove = function() {
        $rootScope.showSpinner = true;
        /*var successCallback = function() {
            flash.setMessage({'type': 'error', 'text': 'The repetition was deleted.'});
            $location.path("/Repetitions");
        };
        var errorCallback = function(response) {
            if(response && response.data && response.data.message) {
                flash.setMessage({'type': 'error', 'text': response.data.message}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        }; 
        $scope.repetition.$remove(successCallback, errorCallback);*/
    	var RepetitionId = {RepetitionId:$routeParams.RepetitionId};
    	RepetitionResource.supprimer(RepetitionId.RepetitionId).then(function(data){
    		console.log("success total");
    		$location.path("/Repetitions");
		
			},function(error){
				console.log("erreur");
				if(error && error.data && error.data.message) {
	                flash.setMessage({'type': 'error', 'text': error.data.message}, true);
	            } else {
	                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
	            }
		});
    };
    
    $scope.statutList = [
        "annule",  
        "valide",  
        "attente"  
    ];
    $scope.niveau_scolaire_repetiteurList = [
        "BACCALAUREAT",  
        "BTS",  
        "LICENCE",  
        "MASTER",  
        "DOCTORAT"  
    ];
    $scope.$watch("parentSelection", function(selection) {
        if (typeof selection != 'undefined') {
            $scope.repetition.parent = {};
            $scope.repetition.parent.id = selection.value;
        }
    });
    
    $scope.get();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});