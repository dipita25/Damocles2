

angular.module('damocles').controller('NouveauPlanningRepetiteurController', function($scope,$window,$rootScope, $routeParams,$window, $location, flash
		,MatiereResource,Matiere_PlanningResource,NouveauPlanningResource,NouvelleSeanceResource ) {
    var self = this;
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.ApprenantDTO = $scope.ApprenantDTO || {};
    $scope.utilisateurDTO = $scope.utilisateurDTO || {};
    $scope.seanceDTO = $scope.seanceDTO || {};

    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.ApprenantsResults = [];
    $scope.searchMatieresResults = [];
    $scope.searchApprenantsResults = [];
    $scope.ApprenantDTO = $scope.ApprenantDTO || {};
    
    $scope.searchMatieresResults = MatiereResource.queryAll();
    
     
    $scope.utilisateurDTO.id_personne = $window.localStorage.getItem("id_personne");
    
    $scope.save = function(){
    	$rootScope.showSpinner = true;
    	
    	$scope.seanceDTO.id_apprenant = $scope.ApprenantDTO.id;
    	console.log($scope.seanceDTO.id_apprenant);
    	console.log($scope.seanceDTO);
    	
    	NouvelleSeanceResource.nouveau($scope.seanceDTO).then(function(data){
    		console.log("success total");
    		
    	},function(error){
    		console.log("erreur");
        	
        });
    };
    
    $scope.ajusterMatiere = function(resultat){
    	$rootScope.showSpinner = true;
    	
    	document.getElementById("saveSeance").enabled = true ;
    	$scope.ApprenantDTO.id = resultat.id;
    	Matiere_PlanningResource.mesMatieres($scope.ApprenantDTO).then(function(data){
    		console.log("success total");
    		$scope.searchMatieresResults = data;
    		
    	},function(error){
    		console.log("erreur");
        	
        });
    }
    
    var recherche = function(){

    	$rootScope.showSpinner = true;
    	
    	NouveauPlanningResource.mesApprenants($scope.utilisateurDTO).then(function(data){
    		console.log("success total");
    		$scope.ApprenantsResults = data;
    		
    	},function(error){
    		console.log("erreur");
        	
        });
    }
    
    $scope.cancel = function() {
		$location.path("/Repetiteur/planning");
	}
    
    recherche();
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});