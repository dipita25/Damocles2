
angular.module('damocles').controller('NewFeedbackController', function ($scope,$window,$rootScope, $location, locationParser, flash, FeedbackResource , SeanceResource, UtilisateurResource) {
    $scope.disabled = false;
    $scope.$location = $location;
    $scope.feedback = $scope.feedback || {};
    
    if($rootScope.authenticated == false){
    	$window.localStorage.removeItem("id_personne");
    	$window.localStorage.removeItem("type_utilisateur");
    	$rootScope.authenticated = false;
		$rootScope.parent = false;
		$rootScope.repetiteur = false;
		$rootScope.admin = false;
		
		$location.path("#/");
    }
    
    $scope.seanceList = SeanceResource.queryAll(function(items){
        $scope.seanceSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.id
            });
        });
    });
    $scope.$watch("seanceSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.feedback.seance = {};
            $scope.feedback.seance.id = selection.value;
        }
    });
    
    $scope.auteurList = UtilisateurResource.queryAll(function(items){
        $scope.auteurSelectionList = $.map(items, function(item) {
            return ( {
                value : item.id,
                text : item.id
            });
        });
    });
    $scope.$watch("auteurSelection", function(selection) {
        if ( typeof selection != 'undefined') {
            $scope.feedback.auteur = {};
            $scope.feedback.auteur.id = selection.value;
        }
    });
    

    $scope.save = function() {
        var successCallback = function(data,responseHeaders){
            var id = locationParser(responseHeaders);
            flash.setMessage({'type':'success','text':'The feedback was created successfully.'});
            $location.path('/Feedbacks');
        };
        var errorCallback = function(response) {
            if(response && response.data) {
                flash.setMessage({'type': 'error', 'text': response.data.message || response.data}, true);
            } else {
                flash.setMessage({'type': 'error', 'text': 'Something broke. Retry, or cancel and start afresh.'}, true);
            }
        };
        FeedbackResource.save($scope.feedback, successCallback, errorCallback);
    };
    
    $scope.cancel = function() {
        $location.path("/Feedbacks");
    };
    
    
    /* garde la session du user connecté et eviter de le deconnecter lorsque la page est rafraichie*/
    $scope.$watch('$viewContentLoaded', function(){
    	// traitement à effectuer au chargement de la page
    	var id_personne = $window.localStorage.getItem("id_personne");
    	if($window.localStorage.getItem("type_utilisateur")){
    		
        	var type_utilisateur = $window.localStorage.getItem("type_utilisateur");
    		if(type_utilisateur == "admin"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.repetiteur = false;
				$rootScope.admin = true;
    		}
    		if(type_utilisateur == "parent"){
    			$rootScope.authenticated = true;
				$rootScope.admin = false;
				$rootScope.repetiteur = false;
				$rootScope.parent = true;
    		}
    		if(type_utilisateur == "repetiteur"){
    			$rootScope.authenticated = true;
				$rootScope.parent = false;
				$rootScope.admin = false;
				$rootScope.repetiteur = true;
    		}
    	}
    });
});