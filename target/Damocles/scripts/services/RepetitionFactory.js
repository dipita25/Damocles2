/*angular.module('damocles').factory('RepetitionResource', function($resource){
    var resource = $resource('rest/repetitions/:RepetitionId',{RepetitionId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});*/

function RepetitionResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.queryAll = function () {
        var d = $q.defer();
        var url = "rest/repetitions";
        $http.get(url).then(function (response) {
            var items = response.data;
            d.resolve(items);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.query = function (id) {
    	var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/repetitions/get",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.supprimer = function (id) {
    	var d = $q.defer();
        $http({
            method: 'DELETE',
            url: "rest/repetitions/delete",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.update = function (entity) {
    	var d = $q.defer();
        $http({
            method: 'PUT',
            url: "rest/repetitions/update",
            data: entity,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    return service;
}


angular.module('damocles').factory('RepetitionResource', ['$q', '$http', '$rootScope', '$timeout', RepetitionResource]);