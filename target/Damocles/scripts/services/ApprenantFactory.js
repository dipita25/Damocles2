/*angular.module('damocles').factory('ApprenantResource', function($resource){
    var resource = $resource('rest/apprenants/:ApprenantId',{ApprenantId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});*/

function ApprenantResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.queryAll = function () {
        var d = $q.defer();
        var url = "rest/apprenants";
        $http.get(url).then(function (response) {
            var items = response.data;
            d.resolve(items);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.update = function (entity) {
    	var d = $q.defer();
        $http({
            method: 'PUT',
            url: "rest/apprenants/update",
            data: entity,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.query = function (id) {
    	var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/apprenants/get",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.supprimer = function (id) {
    	var d = $q.defer();
        $http({
            method: 'DELETE',
            url: "rest/apprenants/delete",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    return service;
}

angular.module('damocles').factory('ApprenantResource', ['$q', '$http', '$rootScope', '$timeout', ApprenantResource]);