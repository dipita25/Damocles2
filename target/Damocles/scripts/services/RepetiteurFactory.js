/*angular.module('damocles').factory('RepetiteurResource', function($resource){
    var resource = $resource('rest/repetiteurs/:RepetiteurId',{RepetiteurId:'@id'},{'queryAll':{method:'GET',isArray:true},'query':{method:'GET',isArray:false},'update':{method:'PUT'}});
    return resource;
});*/

function RepetiteurResource($q, $http, $rootScope, $timeout) {
    var service = {};
    
    service.addToRepetition = function (entity) {
    	var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/repetiteurs/addRepetition",
            data: entity,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.create = function (entity) {
    	var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/repetiteurs/create",
            data: entity,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };

    service.queryAll = function () {
        var d = $q.defer();
        var url = "rest/repetiteurs";
        $http.get(url).then(function (response) {
            var items = response.data;
            d.resolve(items);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.query = function (id) {
    	var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/repetiteurs/get",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.supprimer = function (id) {
    	var d = $q.defer();
        $http({
            method: 'DELETE',
            url: "rest/repetiteurs/delete",
            data: id,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    service.update = function (entity) {
    	var d = $q.defer();
        $http({
            method: 'PUT',
            url: "rest/repetiteurs/update",
            data: entity,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                    $rootScope.showSpinner = false;
                });

        return d.promise;

    };
    
    return service;
}


angular.module('damocles').factory('RepetiteurResource', ['$q', '$http', '$rootScope', '$timeout', RepetiteurResource]);