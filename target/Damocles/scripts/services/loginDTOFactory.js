/*angular.module('damocles').factory('LoginDTOResource', function($resource){
	var resource = $resource('rest/login',null,{'login':{method:'POST',isArray:false}});
	
    return resource;
});*/

function LoginDTOResource($q, $http, $rootScope, $timeout) {
    var service = {};

    service.login = function (loginDTO) {
        var d = $q.defer();
        $http({
            method: 'POST',
            url: "rest/login",
            data: loginDTO,
            headers: {'Content-Type': 'application/json'}
        }).then(function (response) {
            d.resolve(response.data);
            $rootScope.showSpinner = false;
        },
                function (error) {
                    d.reject(error);
                	$rootScope.showSpinner = false;
                });
        return d.promise;
    };
    
    return service;
}


angular.module('damocles').factory('LoginDTOResource', ['$q', '$http', '$rootScope', '$timeout', LoginDTOResource]);